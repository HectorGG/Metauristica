
package Simulador;

import EstructuraDatos.Tarea;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Queue;

/**
 *
 * @author Hector Gonzalez Guerreiro
 * @since  13/03/2018
 * 
 * El simulador consta de una tabla de cola llamada trabajo, esta contiene informacion
 * proporcionada por el dataSet cada elemento de la table corresponde con una cola de
 * tareas ordenada de mayor prioridad a menor.
 * 
 * El atributo maquina corresponde con las colas de las maquinas, el contenido de estas
 * colas corresponde con las tareas que corresponde a dicha maquina y estan esperando 
 * a que la maquina les atienda.
 * 
 * El atributo solucion candidata representa el orden de llegada que debe tener los 
 * trabajos para que se obtenga un tiempo x en el cual se termine todos los trabajos.
 * 
 * La tabla progreso representa cuantas tareas lleva cada trabajo.
 * 
 * Numero maximo de maquinas sin utilizar, timpo en el que todas las maquinas se 
 * estan utilizando.
 */


public class Simulador {
    private  ArrayList<Queue<Tarea>> trabajos = new ArrayList<>(); 
    private  ArrayList<Queue<Tarea>> maquinas = new ArrayList<>();
    private  int [] solucionCandidata;
    private  int [] progreso;
    private  int [] tamaCola; 
    private int numJobs,numMaquinas,iteraciones = 0;
    private int maxOutMachine, maxTimeUtilMachine;
    
    /**
     * A partir de la solucion candidata se genera una simulacion que nos determinara
     * el tiempo que nos llevara realizar todos los trabajos en el orden que se estipula.
     * 
     * @param solucionCandidata
     * @param TR
     * @param numJobs
     * @param numMachine
     */
    public void Simular(int [] solucionCandidata, ArrayList<Queue<Tarea>> TR, int numJobs, int numMachine){
        this.solucionCandidata = solucionCandidata;
        this.progreso = new int[numJobs];
        this.tamaCola = new int[numMachine];
        this.numJobs = numJobs;
        this.numMaquinas = numMachine;
        int contadorEspera,contadorUso;
        
        maquinas = new ArrayList<>();
        trabajos = new ArrayList<>();
        for(int index = 0; index < TR.size(); index++) trabajos.add(new ArrayDeque<>(TR.get(index)));
        
        iteraciones = 0;
        
        for(int index = 0; index < numJobs; index++) this.progreso[index] = 0;
        for(int index = 0; index < numMachine; index++) this.maquinas.add(new ArrayDeque<>());
        for(int index = 0; index < numMachine; index++) this.tamaCola[index] = numJobs;
        
        for (int i : solucionCandidata) {
            this.maquinas.get(this.trabajos.get(i).peek().getMaquina()).add(this.trabajos.get(i).peek()); 
            this.trabajos.get(i).remove();
        }
        
        while(!vacio(this.maquinas)){
            contadorUso = 0;
            contadorEspera = 0;
            for(int maquina = 0; maquina < numMaquinas; maquina++){
                if(!this.maquinas.get(maquina).isEmpty()){
                    if(this.maquinas.get(maquina).peek().getOrden() == this.progreso[this.maquinas.get(maquina).peek().getTrabajo()]){
                        if(this.maquinas.get(maquina).peek().getTime() > 0){
                            this.maquinas.get(maquina).peek().setTime(this.maquinas.get(maquina).peek().getTime() - 1);
                            if(this.maquinas.get(maquina).peek().getTime() == 0) {
                                this.progreso[this.maquinas.get(maquina).peek().getTrabajo()] = this.progreso[this.maquinas.get(maquina).peek().getTrabajo()] + 1;
                                this.maquinas.get(maquina).remove();
                                this.tamaCola[maquina] = this.tamaCola[maquina] - 1;
                            }
                        }
                        else{
                            this.progreso[this.maquinas.get(maquina).peek().getTrabajo()] = this.progreso[this.maquinas.get(maquina).peek().getTrabajo()] + 1;
                            this.maquinas.get(maquina).remove();
                            this.tamaCola[maquina] = this.tamaCola[maquina] - 1;
                        }
                    }
                    else contadorEspera++;    
                }
            }
            // Analisis de la iteracions //
            if(this.maxOutMachine < contadorEspera) this.maxOutMachine = contadorEspera;
            if(this.maxTimeUtilMachine < contadorUso) this.maxTimeUtilMachine = contadorUso;
            //System.out.println(iteraciones);
            iteraciones++;
        }
    }
    
     /**
     * Comprueba si en queda trabajos aun por asignar o trabajar con el.
     * @param Cola representa la estructura de datos que contiene los trabajos.
     * @return <b> FALSE </B> si no esta vacio y aun contiene tareas. <b> TRUE </b> si esta vacio.
     */
    public  boolean vacio(ArrayList<Queue<Tarea>> Cola){
        int index = 0;
        while(index < Cola.size()){
            //if(!Cola.get(index).isEmpty()) return false;
            if(this.tamaCola[index] != 0) return false;
            index++;
        }
        return true;
      
    }
    
    public int getIteraciones() {
        return iteraciones;
    }

    public int getMaxOutMachine() {
        return maxOutMachine;
    }

    public int getMaxTimeUtilMachine() {
        return maxTimeUtilMachine;
    }

    public int[] getSolucionCandidata() {
        return solucionCandidata;
    }
      
}
