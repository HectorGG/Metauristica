
package ALG;

import EstructuraDatos.Movimiento;
import EstructuraDatos.Resultados;
import EstructuraDatos.Tarea;
import java.util.ArrayList;
import java.util.Queue;
import javafx.util.Pair;

/**
 *
 * @author Hector Gonzalez Guerreiro.
 * 
 * Este algoritmo de busqueda local genera vecinos hasta que el vecino que genera el mejor que 
 * el cadidato actual, con lo cual deja de generar mas vecinos y lo sustituye como solucion 
 * actual, este algoritmo es muchisimo mas rapido que el de busquedaLocal.
 * 
 */

public class BusquedaLocalPM extends Algoritmos{
    private String nombre = "BusquedaLocalPV"; 
    private final int id;
    private int porcentage;
    private ArrayList<Movimiento> memoria = new ArrayList<>();
    
    public BusquedaLocalPM(int id, int porcentage){
        super();
        this.id = id;
        this.porcentage = porcentage;
    }
    
     /**
     * Esta funcion permite la generacion de candidatos segun el algoritmo que
     * estamos usando.
     * @param T Represena la lista de trabajos.
     * @return Un conjunto candidato para una solucion.
     */
    
    @Override
    public int[] generarCandidatos(ArrayList<Queue<Tarea>> T) {
        int[] conjuntoSolucion = new int[numJobs * numMaquinas];
        int num = 0;
        while (!this.vacio(T)) {
            int aleatorio = rd.nextInt(numJobs);
            if (!T.get(aleatorio).isEmpty()) {
                conjuntoSolucion[num] = T.get(aleatorio).poll().getTrabajo();
                num++;
            }
        }
        return conjuntoSolucion;
    }
     

    @Override
    public Resultados call() {
        long startTime,finalTime;
        startTime = System.currentTimeMillis();

        int iteracionesMejor, iteracionesActual;
        int [] Solucion = generarCandidatos((ArrayList<Queue<Tarea>>)this.data.getColaTrabajos().clone());
        int llamdaSimular = 1;
        s.Simular(Solucion, this.data.getColaTrabajos(), numJobs, numMaquinas);
        iteracionesMejor = s.getIteraciones();
        
        do{
            int index = 0;
            iteracionesActual = iteracionesMejor;
            ArrayList<Movimiento> listaVecinos = this.generadorVecinos(Solucion);
            memoria.clear();
            while(index < listaVecinos.size()){
                s.Simular(this.two_ops(Solucion, listaVecinos.get(index).getIzq(), listaVecinos.get(index).getDer()), (ArrayList<Queue<Tarea>>)this.data.getColaTrabajos().clone(), numJobs, numMaquinas);
                llamdaSimular++;
                if(s.getIteraciones() < iteracionesMejor){
                    iteracionesMejor = s.getIteraciones();
                    Solucion = s.getSolucionCandidata();
                }
                index++;
            }
            if(llamdaSimular%100 == 0)dat.add(new Pair<>(id,new Pair<>(iteracionesMejor,llamdaSimular)));
        } while(iteracionesActual > iteracionesMejor);
        
        finalTime = System.currentTimeMillis() - startTime;
        return new Resultados(iteracionesMejor, llamdaSimular, finalTime);
    }
    
    
    public ArrayList<Movimiento> generadorVecinos(int [] ConjuntoSolucion){
        int max = ((ConjuntoSolucion.length*ConjuntoSolucion.length) - (2 * ConjuntoSolucion.length));
        float numVecinos = (float) (max * (porcentage / 100.0));
        System.out.println(numVecinos);
        System.out.println(max);
        ArrayList<Movimiento> salida = new ArrayList<>();
        
        for(int index = 0; index < numVecinos; index++){
            Movimiento mv;
            do{
                mv = new Movimiento(rd.nextInt(ConjuntoSolucion.length), rd.nextInt(ConjuntoSolucion.length),0);
                
            }while(this.memoria.contains(mv) || mv.getDer() == mv.getIzq());
            this.memoria.add(mv);
            salida.add(mv);
            
        }
        return salida;
    }
    
}
