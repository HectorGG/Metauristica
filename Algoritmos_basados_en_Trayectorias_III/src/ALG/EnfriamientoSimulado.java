package ALG;

import EstructuraDatos.Resultados;
import EstructuraDatos.Tarea;
import Simulador.Greddy;
import java.util.ArrayList;
import java.util.Queue;
import javafx.util.Pair;

/**
 *
 * @author Hector Gonzalez Guerreiro
 *
 * Algoritmo de enfriamiento simulado. Esquema de enfriamiento en el siguiente
 * Tk = T0/( 1 + k); La velocidad de enfriamiento L(T) = 20 La temperatura
 * inicial sera T0 = mu/-log(fi) * coste de la solucion inicial.
 *
 */
public class EnfriamientoSimulado extends Algoritmos {

    private final int numSolucionesGeneradas;
    private final int numIteraciones;
    private final double fi;
    private double TemperaturaInicial;

    private final ArrayList<int[]> memoria = new ArrayList<>();
    String nombre = "Enfriamiento Simulado"; 
    private final int id;
    
    public EnfriamientoSimulado(int id, int numSGeneradas, double fi, int numIteraciones) {
        this.numSolucionesGeneradas = numSGeneradas;
        this.numIteraciones = numIteraciones;
        this.fi = fi;
        this.id = id;
    }

   @Override
    public int[] generarCandidatos(ArrayList<Queue<Tarea>> cola) {
        Greddy gr = new Greddy();
        gr.Greedy(cola, numJobs, numMaquinas);
        return gr.getSolucionCandidata();

    }
    

    @Override
    public Resultados call() {
        long startTime,finalTime;
        startTime = System.currentTimeMillis();
        
        int[]  SolucionActual, SolucionCandidata;
        int factorCoste, costeActual, costeCandidata, contador = 0;
        double condicionFriamientoParada;
        int llamadaSimulador = 1;
        
        SolucionActual = this.generarCandidatos(this.data.getColaTrabajos());
        this.s.Simular(SolucionActual, this.data.getColaTrabajos(), numJobs, numMaquinas);
        costeActual = this.s.getIteraciones();
        System.out.println(costeActual);
        
        condicionFriamientoParada = this.TemperaturaInicial = (this.fi / -(Math.log(fi))) * costeActual;
        
        while (contador < numIteraciones * numJobs) {
            for (int index = 0; index < numSolucionesGeneradas; index++) {
                SolucionCandidata = this.seleccionSolucion(SolucionActual);
                this.s.Simular(SolucionCandidata, this.data.getColaTrabajos(), numJobs, numMaquinas);
                llamadaSimulador++;
                
                costeCandidata = this.s.getIteraciones();
                factorCoste = costeCandidata - costeActual;
               
                float pos = rd.nextFloat();
                if (pos < Math.exp( -fi / condicionFriamientoParada) || factorCoste < 0) {
                    SolucionActual = SolucionCandidata;
                    costeActual = this.s.getIteraciones();
                }
               
            }
            if(llamadaSimulador%100 == 0)dat.add(new Pair<>(id,new Pair<>(costeActual,llamadaSimulador)));
            condicionFriamientoParada = condicionFriamientoParada / (1 + contador);
            contador++;
        }
        
        
        return new Resultados(costeActual, llamadaSimulador, System.currentTimeMillis() - startTime);
    }

    /**
     * Seleccion de Solucion. Este generador en especifico generara un candidato
     * aletorio, intercambiando las posiciones aleatorio, simpre y cuando no sea
     * el mismo y no se repita.
     *
     * @param Solucion_actual
     * @return
     */
    private int[] seleccionSolucion(int[] Solucion_actual) {
        int index1;
        int index2;
        int salida[] = null;

        memoria.add(Solucion_actual);

        do {
            index1 = rd.nextInt(Solucion_actual.length);
            index2 = rd.nextInt(Solucion_actual.length);
        
            salida = new int[Solucion_actual.length];
            System.arraycopy(Solucion_actual, 0, salida, 0, Solucion_actual.length);
            int aux = salida[index1];
            salida[index1] = salida[index2];
            salida[index2] = aux;

        } while (memoria.contains(salida));

        return salida;
    }

    /**
     * Genera una solucion, Esta solucion se genera dividiendo el vector en 2 partes.
     * entres estos vectores se intercambian un elemento.
     * @param Solucion_actual
     * @param nivel
     * @return solucion candidata.
     */
    private int[] seleccionSolucion(int[] Solucion_actual, int nivel) {
        int index1 = nivel % Solucion_actual.length / 2;
        int index2 = ((nivel % Solucion_actual.length) + Solucion_actual.length/2) % Solucion_actual.length;
        return this.two_ops(Solucion_actual, index1, index2);
    }

}
