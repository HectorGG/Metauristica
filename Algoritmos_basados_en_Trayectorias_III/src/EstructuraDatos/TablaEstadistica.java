package EstructuraDatos;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Hector Gonzalez Guerreiro.
 * @since 31/03/2018.
 * 
 * Es la clase encargada de guardar datos y mostrarlo por una ventana.
 * los datos que muestra son el nombre del algoritmo que se esta usando y el tiempo
 * del simulador.
 */
public class TablaEstadistica extends AbstractTableModel{
    private final ArrayList<info> datos = new ArrayList<>();
    private final String [] column = {" Fichero "," Algoritmo ","seeds"," Tiempo Solucion "," Evaluacion ", "CPU"};
    public class info {
        public String fichero;
        public String name;
        public long seeds;
        public int time;
        public int Eva;
        public double CPU;

        public info(String fichero, String name,long seeds, int time, int Eva, double CPU) {
            this.fichero = fichero;
            this.name = name;
            this.time = time;
            this.Eva = Eva;
            this.CPU = CPU;
            this.seeds = seeds;
        }

        public String getFichero() {
            return fichero;
        }

        public String getName() {
            return name;
        }

        public int getTime() {
            return time;
        }

        public int getEva() {
            return Eva;
        }
        
        public double getCPU(){
            return CPU;
        }
        public long getSeeds(){
            return seeds;
        }

        
 
    }
    
    @Override
    public int getRowCount() {
        return this.datos.size();
    }
    
      @Override
    public String getColumnName(int columnIndex) {
        return this.column[columnIndex];
    }

    @Override
    public int getColumnCount() {
        return this.column.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex){
            case 0: return this.datos.get(rowIndex).fichero;
            case 1: return this.datos.get(rowIndex).name;
            case 2: return this.datos.get(rowIndex).seeds;
            case 3: return this.datos.get(rowIndex).time;
            case 4: return this.datos.get(rowIndex).Eva;
            case 5: return this.datos.get(rowIndex).CPU;
            default: return null;
        }
    }
    
    public void add(String filer, String name, int time, long seeds, int eva,double CPU ){
        this.datos.add(new info(filer,name,seeds,time ,eva, CPU));
        fireTableRowsInserted(this.datos.size()-1, this.datos.size()-1);
    }
    
}
