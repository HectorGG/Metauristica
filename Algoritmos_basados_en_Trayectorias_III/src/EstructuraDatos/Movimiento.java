/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EstructuraDatos;

/**
 *
 * @author 
 */
public class Movimiento {

    private final int Izq, Der, Coste;

    public Movimiento(int Izq, int Der, int Coste) {
        this.Izq = Izq;
        this.Der = Der;
        this.Coste = Coste;
    }

    public int getIzq() {
        return Izq;
    }

    public int getDer() {
        return Der;
    }

    public int getCoste() {
        return Coste;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + this.Izq;
        hash = 61 * hash + this.Der;
        hash = 61 * hash + this.Coste;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        Movimiento ob = (Movimiento) obj; 
        return (this.Izq == ob.Izq && this.Der == ob.Der) || (this.Der == ob.Izq && this.Izq == ob.Der);
    }

}
