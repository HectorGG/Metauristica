/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EstructuraDatos;

/**
 *
 * @author Hector Gonzalez Guerreiro
 * Clase encargada sacar los datos de los algoritmos.
 * 
 */
public class Resultados {
    private final int Coste;
    private final int Evaluaciones;
    private final double tiempoCPU;

    public Resultados(int Coste, int Evaluaciones, double tiempoCPU) {
        this.Coste = Coste;
        this.Evaluaciones = Evaluaciones;
        this.tiempoCPU = tiempoCPU;
    }

    public int getCoste() {
        return Coste;
    }

    public int getEvaluaciones() {
        return Evaluaciones;
    }

    public double getTiempoCPU() {
        return tiempoCPU;
    }
   
   
}
