
package EstructuraDatos;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Queue;
import java.util.StringTokenizer;

/**
 *
 * @author Hector Gonzalez Guerreiro.
 *
 * Clase encargada de la gestion de la leectura de los dataset. para ello lo
 * almacenamos en una tabla dinamica de colas circulares, cada cola representa
 * el conjunto de tareas que pose un trabajo y cada posicion de esa lista
 * corresponde a un trabajo.
 *
 *
 */
public class DataSet {

    private final ArrayList<Queue<Tarea>> ColaTrabajos;
    private final int numJobs;
    private final int numMaquinas;

    /**
     * Constructor, encargado de cargar la informacion de un dataSet 
     * @param filer Nombre del fichero
     * @throws FileNotFoundException El fichero no existe.
     * @throws IOException
     */
    public DataSet(String filer) throws FileNotFoundException, IOException {
        int orden = 0;
        StringTokenizer token, token2;
        FileReader jobFiler = new FileReader(filer);
        FileReader machineFiler = new FileReader(filer);

        BufferedReader job = new BufferedReader(jobFiler);
        BufferedReader machin = new BufferedReader(machineFiler);

        job.readLine();
        token = new StringTokenizer(job.readLine());
        numJobs = Integer.valueOf(token.nextToken());
        numMaquinas = Integer.valueOf(token.nextToken());
        job.readLine();
        for (int salto = 0; salto < numJobs + 4; salto++) {
            machin.readLine();
        }
      
        this.ColaTrabajos = new ArrayList<>();

        for (int index = 0; index < this.numJobs; index++) {
            token = new StringTokenizer(job.readLine());
            token2 = new StringTokenizer(machin.readLine());
            
            this.ColaTrabajos.add(new ArrayDeque<>());
            
            while (token.hasMoreElements()) {
                this.ColaTrabajos.get(index).add(new Tarea(Integer.valueOf(token.nextToken()), Integer.valueOf(token2.nextToken()) - 1, index, orden));
                orden++;
            }
            orden = 0;

        }
    }
    
    /**
     * Devuelve una copia de la lista circular.
     * @return Cola Circular.
     */
    public ArrayList<Queue<Tarea>> getColaTrabajos() {
        ArrayList<Queue<Tarea>> Salida = new ArrayList<>();
        for(int index = 0; index < this.ColaTrabajos.size(); index++){
            Queue<Tarea> qu = this.ColaTrabajos.get(index);
            Iterator<Tarea> iter =  (Iterator<Tarea>)qu.iterator();
            Salida.add(new ArrayDeque<>());
            while(iter.hasNext()){
                Salida.get(Salida.size() - 1).add(iter.next().clone());
            }
            
        } 
            
        return (ArrayList<Queue<Tarea>>) Salida.clone();
    }

    public int getNumJobs() {
        return numJobs;
    }

    public int getNumMaquinas() {
        return numMaquinas;
    }

}
