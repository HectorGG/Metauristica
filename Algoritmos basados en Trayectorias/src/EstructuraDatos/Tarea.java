
package EstructuraDatos;

/**
 *
 * @author Hector Gonzalez Guerreiro.
 * @since 13/3/2018
 * Una tarea se compone de varias campos.
 * <h2> Maquina </h2> Guarda el id de a que maquina le corresponde la tarea.
 * <h2> time </h2> Almacena cuanto tiempo le cuesta la maquina terminar susodicho tarea.
 * <h2> trabajo </h2> Indica a que trabajo pertenece la tarea.
 * <h2> orden </h2> Indica en que orden debe ejecutarse dentro del trabajo.
 * 
 */
public class Tarea {
    private int maquina, time, trabajo, orden;
    
    public Tarea(int _time, int _maquina, int _trabajo, int _orden){
        this.maquina = _maquina;
        this.time = _time;
        this.orden = _orden;
        this.trabajo = _trabajo;
    }

    public int getMaquina() {
        return maquina;
    }

    public void setMaquina(int maquina) {
        this.maquina = maquina;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public int getTrabajo() {
        return trabajo;
    }

    public void setTrabajo(int trabajo) {
        this.trabajo = trabajo;
    }

    public int getOrden() {
        return orden;
    }

    public void setOrden(int orden) {
        this.orden = orden;
    }

    @Override
    public String toString() {
        return "Tarea{" + "Maquina=" + maquina + ", time=" + time + ", trabajo=" + trabajo + ", orden=" + orden + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + this.maquina;
        hash = 43 * hash + this.trabajo;
        hash = 43 * hash + this.orden;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Tarea other = (Tarea) obj;
        if (this.maquina != other.maquina) {
            return false;
        }
        if (this.trabajo != other.trabajo) {
            return false;
        }
        if (this.orden != other.orden) {
            return false;
        }
        return true;
    }

   

    
    @Override
    public Tarea clone(){
        Tarea T = new Tarea(time, maquina, trabajo, orden);
        return T;
    }
    
    
   
    
    
}
