
package Simulador;

import EstructuraDatos.Tarea;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;
import javafx.util.Pair;

/**
 *
 * @author Hector Gonzalez Guerreiro
 * @since  13/03/2018
 * 
 * El algoritmo Greddy es un algorimo voraz que genera una solucion candidata.
 * Este algorimot escoge aquellas tareas que permitan el funcionamiento el maximo tiempo posible de las maquinas
 * 
 */


public class Greddy {
    private ArrayList<ArrayList<Tarea>> maquinas = new ArrayList<>();
    private final LinkedList<Integer> solucion = new LinkedList<>();
    private Tarea [] TareasEnMauinas = new Tarea[this.numMaquinas];
    private int [] progreso;
    private int numJobs, numMaquinas,iteraciones;
    private int maxOutMachine, maxTimeUtilMachine;
    
    /**
     * A partir de la solucion candidata se genera una simulacion que nos determinara
     * el tiempo que nos llevara realizar todos los trabajos en el orden que se estipula.
     * 
     * @param TR
     * @param numJobs
     * @param numMachine
     */
    public void Greedy(ArrayList<Queue<Tarea>> TR, int numJobs, int numMachine){
        this.progreso = new int[numJobs];
        this.TareasEnMauinas = new Tarea[numJobs];
        this.numJobs = numJobs;
        this.numMaquinas = numMachine;
        maquinas = new ArrayList<>();
        iteraciones = 0;
        
        for(int index = 0; index < numJobs; index++) this.progreso[index] = 0;
        
        for(int index = 0; index < numMachine; index++) this.maquinas.add(new ArrayList<>());
        
        for(int index = 0; index < TR.size(); index++){
            while(!TR.get(index).isEmpty()){
                this.maquinas.get(TR.get(index).peek().getMaquina()).add(TR.get(index).peek());
                TR.get(index).remove();
            }
        }
        
        while(!this.vacio(this.maquinas)){
            
            for(int indexMaquina = 0; indexMaquina < numMaquinas; indexMaquina++){
                if(this.TareasEnMauinas[indexMaquina] != null && this.TareasEnMauinas[indexMaquina].getTime() > 0){
                    this.TareasEnMauinas[indexMaquina].setTime(this.TareasEnMauinas[indexMaquina].getTime() - 1);
                    if(this.TareasEnMauinas[indexMaquina].getTime() == 0) this.progreso[this.TareasEnMauinas[indexMaquina].getTrabajo()] = this.progreso[this.TareasEnMauinas[indexMaquina].getTrabajo()] + 1;
                }
                else{
                    
                    if(!this.maquinas.get(indexMaquina).isEmpty()){
                    /*Estudio la lista de la maquina */
                    /*Busco en la lista de las maquinas la primera tarea que se me permita ejecutar en el orden correcto */
                    int elementoPonerMaquina = this.posicionTareaMaquina(this.maquinas.get(indexMaquina), progreso);
                    
                    /*Puede ocurrir que no se pueda poner en ninguna maquina porque esta ya este ocupada o no se posea las tareas adecuadas*/
                    
                        if(elementoPonerMaquina >= 0){
                            this.solucion.add(this.maquinas.get(indexMaquina).get(elementoPonerMaquina).getTrabajo());
                            this.TareasEnMauinas[indexMaquina] = this.maquinas.get(indexMaquina).get(elementoPonerMaquina);
                            this.maquinas.get(indexMaquina).remove(elementoPonerMaquina);
                             
                        }    
                    }
                        
                }
            }
            iteraciones++;
        }
    }
    
    public void Greedy(ArrayList<Queue<Tarea>> TR , int numJobs, int numMachine, HashMap<Pair<Tarea,Integer>,Integer>  MemoriaLargoPlazo){
      this.progreso = new int[numJobs];
        this.TareasEnMauinas = new Tarea[numJobs];
        this.numJobs = numJobs;
        this.numMaquinas = numMachine;
        iteraciones = 0;
        
        for(int index = 0; index < numJobs; index++) this.progreso[index] = 0;
        
        for(int index = 0; index < numMachine; index++) this.maquinas.add(new ArrayList<>());
        
        for(int index = 0; index < TR.size(); index++){
            while(!TR.get(index).isEmpty()){
                this.maquinas.get(TR.get(index).peek().getMaquina()).add(TR.get(index).peek());
                TR.get(index).remove();
            }
        }
        
        while(!this.vacio(this.maquinas)){
            
            for(int indexMaquina = 0; indexMaquina < numMaquinas; indexMaquina++){
                if(this.TareasEnMauinas[indexMaquina] != null && this.TareasEnMauinas[indexMaquina].getTime() > 0){
                    this.TareasEnMauinas[indexMaquina].setTime(this.TareasEnMauinas[indexMaquina].getTime() - 1);
                    if(this.TareasEnMauinas[indexMaquina].getTime() == 0) this.progreso[this.TareasEnMauinas[indexMaquina].getTrabajo()] = this.progreso[this.TareasEnMauinas[indexMaquina].getTrabajo()] + 1;
                }
                else{
                    
                    if(!this.maquinas.get(indexMaquina).isEmpty()){
                    /*Estudio la lista de la maquina */
                    /*Busco en la lista de las maquinas la primera tarea que se me permita ejecutar en el orden correcto */
                    int elementoPonerMaquina = this.posicionTareaMaquina(this.maquinas.get(indexMaquina), progreso);
                    
                    /*Puede ocurrir que no se pueda poner en ninguna maquina porque esta ya este ocupada o no se posea las tareas adecuadas*/
                    
                        if(elementoPonerMaquina >= 0){
                            this.solucion.add(this.maquinas.get(indexMaquina).get(elementoPonerMaquina).getTrabajo());
                            this.TareasEnMauinas[indexMaquina] = this.maquinas.get(indexMaquina).get(elementoPonerMaquina);
                            this.maquinas.get(indexMaquina).remove(elementoPonerMaquina);
                             
                        }    
                    }
                        
                }
            }
            iteraciones++;
        }
    }
    
    public boolean vacio(ArrayList<ArrayList<Tarea>> lista){
        int index = 0;
        while(index < lista.size()){
            if(!lista.get(index).isEmpty()) return false;
            index++;
        }
        return true;
    }
    
    
    /**
     * Determina la posicion de la tarea dentro de la lista de la maquina que se va ejecutar en 
     * su sodicha maquina.
     * @param maquina Lista de tareas que corre sobre la maquina.
     * @param ordenTrabajos Es el progreso de los trabajos.
     * @return Posicion de una tarea en la lista de las maquinas.
     */
    public int posicionTareaMaquina(ArrayList<Tarea> maquina, int[] ordenTrabajos){
        int timemenor = Integer.MAX_VALUE;
        int salida = -1;
        for(int pos = 0; pos < maquina.size(); pos++){
            if(maquina.get(pos).getOrden() <= ordenTrabajos[maquina.get(pos).getTrabajo()] /*+ (Math.abs(this.numJobs - this.numMaquinas))/2*/&& timemenor > maquina.get(pos).getTime()){
                salida = pos;
                timemenor = maquina.get(pos).getTime();
            }
        }
        
        return salida;
    }
    
      /**
     * Determina la posicion de la tarea dentro de la lista de la maquina que se va ejecutar en 
     * su sodicha maquina.
     * @param maquina Lista de tareas que corre sobre la maquina.
     * @param ordenTrabajos Es el progreso de los trabajos.
     * @param MemoriaLargoPlazo Se le asocia una tabla de frecuencia que expresa la frecuencia de cierta tarea a ser escogida en cierta posicion de la 
     * tabla de maquinas.
     * @return Posicion de una tarea en la lista de las maquinas.
     */
    public int posicionTareaMaquina(ArrayList<Tarea> maquina, int[] ordenTrabajos, HashMap<Pair<Tarea,Integer>,Integer> MemoriaLargoPlazo){
        int timemenor = Integer.MAX_VALUE;
        int frecuencia = Integer.MAX_VALUE;
        int salida = -1;
        ArrayList<Tarea> lista = new ArrayList<>();
        //System.out.println(Arrays.toString(ordenTrabajos));
        for(int pos = 0; pos < maquina.size(); pos++){
            if( frecuencia > MemoriaLargoPlazo.get(new Pair<>(maquina.get(pos),this.solucion.size())) ){
                frecuencia =  MemoriaLargoPlazo.get(new Pair<>(maquina.get(pos),this.solucion.size()));
            }
        }
        
         for(int pos = 0; pos < maquina.size(); pos++){
             if(MemoriaLargoPlazo.get(new Pair<>(maquina.get(pos),this.solucion.size())) == frecuencia){
                 lista.add(maquina.get(pos));
             }
         }
         
         for(int pos = 0; pos < lista.size(); pos++){
              if( lista.get(pos).getOrden() <= ordenTrabajos[lista.get(pos).getTrabajo()] /*+ (Math.abs(this.numJobs - this.numMaquinas))/2*/ && timemenor > maquina.get(pos).getTime() /*&& frecuencia >= MemoriaLargoPlazo.get(lista.get(pos))*/){
                salida = pos;
                timemenor = lista.get(pos).getTime();
            }
         }
        
        
        if(salida >= 0){
            Integer aux = MemoriaLargoPlazo.get(new Pair<>(maquina.get(salida),this.solucion.size()));
            Tarea t = maquina.get(salida);
            MemoriaLargoPlazo.put(new Pair<>(t,this.solucion.size()),aux + 1);
        }
        
        return salida;
    }

    public int getIteraciones() {
        return iteraciones;
    }

    public int getMaxOutMachine() {
        return maxOutMachine;
    }

    public int getMaxTimeUtilMachine() {
        return maxTimeUtilMachine;
    }

    public int [] getSolucionCandidata() {
       int [] salida = new int[this.solucion.size()];
       for(int index = 0; index < this.solucion.size(); index++) salida[index] = this.solucion.get(index);
       return salida;
    }
      
}
