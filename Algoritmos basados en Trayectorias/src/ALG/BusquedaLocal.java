
package ALG;

import EstructuraDatos.Tarea;
import java.util.ArrayList;
import java.util.Queue;
import javafx.util.Pair;

/**
 *
 * @author Hector Gonzalez Guerreiro.
 * @since 31/03/2018
 * 
 * La busqueda local de este tipo generara todas las combinaciones de vecinos posible
 * del tipo 2-ops, de ese conjunto generado se realizar simulaciones para comprobar cuales
 * de ellas es la mejor. No es recomendable usarla en cotas mayores pues tarda demasiado en calcularse.
 * 
 */

public class BusquedaLocal extends Algoritmos{
    String nombre = "BusquedaLocal"; 
    public BusquedaLocal(){
        super();
    }
    
     /**
     * Esta funcion permite la generacion de candidatos segun el algoritmo que
     * estamos usando.
     * @param T Represena la lista de trabajos.
     * @return Un conjunto candidato para una solucion.
     */
    
    @Override
    public int[] generarCandidatos(ArrayList<Queue<Tarea>> T) {
        int[] conjuntoSolucion = new int[numJobs * numMaquinas];
        int num = 0;
        while (!vacio(T)) {
            int aleatorio = rd.nextInt(numJobs);
            if (!T.get(aleatorio).isEmpty()) {
                conjuntoSolucion[num] = T.get(aleatorio).poll().getTrabajo();
                num++;
            }
        }
        return conjuntoSolucion;
    }
    
    /**
     * Genera todos lo vecinos 2-ops que se puede realizar a partir de una solucion.
     * @param Candidato corresponde a la solucion actual.
     * @return lista de candidatos posibles. En este caso en concreto son todos los que se 
     * pueden formar intercambiando 2 posiciones de la tabla. Se optimizado para evitar repeticiones.
     * 
     */
    private int[][] generarVecinos(int [] Candidato){
        int [][] conjuntoVecinos = new int[(Candidato.length * Candidato.length) -  2*Candidato.length][];
        int num = 0, cont = 0, indiceIzq, indiceDer;
        
        while(cont < conjuntoVecinos.length){
            indiceIzq = cont / Candidato.length;
            indiceDer = cont % Candidato.length;
            if(indiceIzq != indiceDer && indiceDer > indiceIzq) {
                conjuntoVecinos[num] = this.two_ops(Candidato, indiceIzq, indiceDer);
                num++;
            }
            cont++;
            
        }
        return conjuntoVecinos;
        
    }
    
    @Override
    public Pair<Integer,Integer> call() {
        int iteracionesMejor,iteracionActual; 
        int [] Solucion = generarCandidatos(this.data.getColaTrabajos());
        int llamadaSimular = 0;
        
        s.Simular(Solucion, this.data.getColaTrabajos(), numJobs, numMaquinas);
        iteracionesMejor = s.getIteraciones();
        
        do{
            iteracionActual = iteracionesMejor;
            
            int [][] conjuntoVecinos = this.generarVecinos(Solucion);
            for (int[] conjuntoVecino : conjuntoVecinos) {
                if (conjuntoVecino != null) {
                    s.Simular(conjuntoVecino, this.data.getColaTrabajos(), numJobs, numMaquinas);
                    llamadaSimular++;
                    if(iteracionesMejor > s.getIteraciones()){
                        iteracionesMejor = s.getIteraciones();
                        Solucion = s.getSolucionCandidata();
                    }
                }
            }
        }while(iteracionActual > iteracionesMejor);
        
        return new Pair<>(iteracionesMejor,llamadaSimular);
    }
    
}
