package ALG;

import EstructuraDatos.Tarea;
import java.util.ArrayList;
import java.util.Queue;
import javafx.util.Pair;

/**
 *
 * @author Hector Gonzalez Guerreiro.
 * @since 04/04/2018
 * 
 * La busqueda Aleatoria genera de forma aleatoria los candidatos, cada candidato se 
 * somete al simulador y se calcula el numero de iteraciones que necesita para terminar 
 * todos los trabajos.
 * 
 */

public class BusquedaAleatoria extends Algoritmos {
    public BusquedaAleatoria() {
        super();
    }

    /**
     * Esta funcion permite la generacion de candidatos segun el algoritmo que
     * estamos usando.
     * @param T Represena la lista de trabajos.
     * @return Un conjunto candidato para una solucion.
     */
    
    @Override
    public int[] generarCandidatos(ArrayList<Queue<Tarea>> T) {
        int[] conjuntoSolucion = new int[numJobs * numMaquinas];
        int num = 0;
        while (!vacio(T)) {
            int aleatorio = rd.nextInt(numJobs);
            if (!T.get(aleatorio).isEmpty()) {
                conjuntoSolucion[num] = T.get(aleatorio).poll().getTrabajo();
                num++;
            }
        }
        return conjuntoSolucion;
    }
    
    @Override
    public Pair<Integer, Integer> call() {
        int veces = 0;
        int llamadaSimular = 0;
        while (veces < 1000 * numMaquinas) {
            int[] candidatos = generarCandidatos(this.data.getColaTrabajos());
            this.s.Simular(candidatos, this.data.getColaTrabajos(), this.data.getNumJobs(), this.data.getNumMaquinas());
            llamadaSimular++;
            
            if (iteracionMejor > s.getIteraciones()) {
                iteracionMejor = s.getIteraciones();
                MejorConjuntoSolucion = s.getSolucionCandidata();
            }
            veces++;
        }
        return new Pair<>(iteracionMejor, llamadaSimular);
    }

}
