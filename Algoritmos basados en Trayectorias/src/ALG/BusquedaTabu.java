
package ALG;

import EstructuraDatos.Tarea;
import Simulador.Greddy;
import Simulador.ListaTabu;
import Simulador.Movimiento;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Queue;
import java.util.Random;
import javafx.util.Pair;

/**
 *
 * @author Hector Gonzalez Guerreiro.
 * 
 * Busqueda Tabu
 * Los elementos necesarios son:
 *  * Memoria a largo plazo, se almacenara los mejores resultados en cada rearanque.
 *  * Memoria a corto plazo sera de un tamaño inicial de 4, este se reducira o se incrementara en un
 *   50% segun una decision aleatoria.
 *  * El numero de vecinos o de movimientos que podemos observar sera de 40, de estos 40 se le aplicara 
 *   el criterio tabu.
 *  * Se usara 3 tipos de rearanques cada uno con un porcentaje para su activacion:
 *      El primero consistira en crearse una solucion inicial.
 *      El segundo en usar unn greddy para generar una solucion usando memoria a largo plazo.
 *      El tercero se usara como inicial la mejor que se obtuvo con anterioridad
 *  *Se usara una estructura de datos que almacena movimientos ListaMovimientos seran una lista con
 *  todos los movimientos posibles
 */
public class BusquedaTabu extends Algoritmos{
   private ListaTabu listaTabu;                                 
   private final HashMap<Pair<Tarea,Integer>,Integer> memoriaLargoPlazo = new HashMap<>();
   private ArrayList<Pair<Integer,Integer>> memoria = new ArrayList<>();
   private int TamañoListaTabu, numMovimientos, numIteraciones;
   private int [] SolucionInicial,SolucionCandidata,Solucion;
   int llamadaSimular = 0;
   
   String nombre = "BusquedaTABU"; 
   private final ArrayList<Pair<Integer,Integer>> ListaMovimientos = new ArrayList<>();
    
    /**
     * 
     * @param numExploracion Es el numero de vecinos que se genera en cada iteracion.
     * @param tamañoListaTabu Es el tamaño inicial de la lista tabu.
     * @param numIteraciones  Es el numero de veces que se busca una solucion entre los vecinos.
     * 
     */
    public BusquedaTabu(int numExploracion, int tamañoListaTabu, int numIteraciones){
        super();
        
        this.numMovimientos = numExploracion;
        this.TamañoListaTabu = tamañoListaTabu;
        this.numIteraciones = numIteraciones;
        
    }
    
     /**
     * Esta funcion permite la generacion de candidatos segun el algoritmo que
     * estamos usando.
     * @param T Represena la lista de trabajos.
     * @return Un conjunto candidato para una solucion.
     */
    
    @Override
    public int[] generarCandidatos(ArrayList<Queue<Tarea>> T) {
        int[] conjuntoSolucion = new int[numJobs * numMaquinas];
        int num = 0;
        while (!vacio(T)) {
            int aleatorio = rd.nextInt(numJobs);
            if (!T.get(aleatorio).isEmpty()) {
                conjuntoSolucion[num] = T.get(aleatorio).poll().getTrabajo();
                num++;
            }
        }
        return conjuntoSolucion;
    }

    @Override
    public Pair<Integer, Integer> call() {
        int reinicializacion = 0;
        Pair<int[],Integer> MejorMovimiento;
        this.inicializar(this.data.getColaTrabajos());
        llamadaSimular = 1;
        
        Greddy Gr = new Greddy();
        Gr.Greedy(this.data.getColaTrabajos(), numJobs, numMaquinas,this.memoriaLargoPlazo);
        this.SolucionInicial = Gr.getSolucionCandidata();
        
        this.s.Simular(SolucionInicial, this.data.getColaTrabajos(), numJobs, numMaquinas);
        MejorMovimiento = this.Mode01(this.s.getSolucionCandidata());
        this.SolucionInicial = this.s.getSolucionCandidata();
        
        do{
            
            /*Calcular cuales de las reinicializaciones debo realizar
             * en el caso de ser menor de 0.50 debo mirar los otros 2 posibles.*/
            this.nuevoTamañoTabu();
            
            if(rd.nextFloat() < 0.5){
                if(rd.nextFloat() < 0.5){
                    /*Se inicia con una busqueda aleatoria*/
                    Pair<int[],Integer> movimientoCandidato = this.Mode01(this.generarCandidatos(this.data.getColaTrabajos()));
                    if(movimientoCandidato.getValue() < MejorMovimiento.getValue()) MejorMovimiento = movimientoCandidato;
                }
                else{
                    /*Se inicia con el mejor candidato hasta el momento */
                    Pair<int[],Integer> movimientoCandidato = this.Mode01(MejorMovimiento.getKey());
                    if(movimientoCandidato.getValue() < MejorMovimiento.getValue()) MejorMovimiento = movimientoCandidato;
                }
            }
            else{
                Gr = new Greddy();
        
                Gr.Greedy(this.data.getColaTrabajos(), numJobs, numMaquinas,this.memoriaLargoPlazo);
                this.SolucionInicial = Gr.getSolucionCandidata();
                this.s.Simular(SolucionInicial, this.data.getColaTrabajos(), numJobs, numMaquinas);
                llamadaSimular++;
                
                Pair<int[],Integer> movimientoCandidato = this.Mode01(this.generarCandidatos(this.data.getColaTrabajos()));
                if(movimientoCandidato.getValue() < MejorMovimiento.getValue()) MejorMovimiento = movimientoCandidato;
                
            }
            reinicializacion++;
            
        }while(reinicializacion < 4);
       return new Pair<>(MejorMovimiento.getValue(),llamadaSimular);
        
    }
       
   /**
     * Seleccion de Solucion. Este generador en especifico generara un candidato
     * aletorio, intercambiando las posiciones aleatorio, simpre y cuando no sea
     * el mismo y no se repita.
     *
     * @param Solucion_actual Conjunto solucion
     * @return Par de posiciones.
     */
    private Pair<Integer,Integer> generadorMovimientos(int[] Solucion_actual) {
        Random rd = new Random(System.currentTimeMillis());
        int index1 = rd.nextInt(Solucion_actual.length);
        int index2 = rd.nextInt(Solucion_actual.length);
        int iteracion = 0;

        while(memoria.contains( new Pair<>(index1,index2)) && iteracion < 100){
            rd = new Random(System.currentTimeMillis());
            index1 = rd.nextInt(Solucion_actual.length);
            index2 = rd.nextInt(Solucion_actual.length);
            iteracion++;
        }
        memoria.add( new Pair<>(index1,index2));
        
    
        return new Pair<>(index1,index2);
    }
    
    /**
     * Genera una especie de busqueda local.
     * @param SolucionInicial
     * @return  El conjunto solucion y el coste de esa solucion.
     */
    private Pair<int [], Integer> Mode01(int [] SolucionInicial){
        int iteraciones = 0;
        Movimiento mejorMovimiento = null;
        int costeMejor = Integer.MAX_VALUE;
        this.listaTabu = new ListaTabu(this.TamañoListaTabu);                  // La lista tabu (Memoria a corto plazo).
        
        while(iteraciones < 8 * this.numJobs){
            memoria = new ArrayList<>();
            Movimiento [] listaVecinos = new Movimiento[numMovimientos];
            int numCandidatosTotales = 0;

            for(int index = 0; index < this.numMovimientos; index++){
               Pair<Integer,Integer> ele = this.generadorMovimientos(SolucionInicial);

               if(!listaTabu.existeMovimiento( new Movimiento(ele.getKey(),ele.getValue(),this.s.getIteraciones()))){
                   this.s.Simular(this.two_ops(SolucionInicial, ele.getKey(), ele.getValue()), this.data.getColaTrabajos(), numJobs, numMaquinas);
                   llamadaSimular++;
                   
                   listaVecinos[index] = new Movimiento(ele.getKey(),ele.getValue(),this.s.getIteraciones());
                   listaTabu.add(listaVecinos[index]);
                   numCandidatosTotales++;
               }

            }

            /**
             * Proceso de seleccion de los mejores candidatos que cumplen las restinciones tabu.
             */

            for(int pos = 0; pos < numCandidatosTotales; pos++){
                if(costeMejor > listaVecinos[pos].getCoste()){
                    costeMejor = listaVecinos[pos].getCoste();
                    mejorMovimiento = new Movimiento(listaVecinos[pos].getIzq(), listaVecinos[pos].getDer(), costeMejor);
                    SolucionInicial = this.two_ops(SolucionInicial, mejorMovimiento.getIzq(), mejorMovimiento.getDer());
                }
            }
            iteraciones++;
        }
        
        return new Pair<>(SolucionInicial,mejorMovimiento.getCoste());
    }
    
    
    private void inicializar(ArrayList<Queue<Tarea>>T){
        for(int index = 0; index < T.size(); index++){
            for(int index2 = 0; index2 < (this.data.getNumJobs() * this.data.getNumMaquinas()); index2++) 
                memoriaLargoPlazo.put(new Pair<>(T.get(index).peek(),index2), 0);
        }
    }
    
    private void nuevoTamañoTabu(){
        float posivilidades = rd.nextFloat();
        
        if(posivilidades <= 0.5 && this.TamañoListaTabu != 0)
            this.TamañoListaTabu = this.TamañoListaTabu/2;
        else 
            this.TamañoListaTabu = this.TamañoListaTabu * 2;
    }
    
}
