
package ALG;

import EstructuraDatos.DataSet;
import EstructuraDatos.Tarea;
import Simulador.Simulador;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.util.Pair;

/**
 *
 * @author Hector Gonzalez Guerreiro
 * @since 30/03/2018
 * Gestor de los algoritmos de busqueda.
 *  
 */
public abstract class Algoritmos implements Callable<Pair<Integer,Integer>>{
    protected int numJobs,numMaquinas;
    protected Simulador s = new Simulador();
    protected DataSet data;
    protected int iteracionMejor = Integer.MAX_VALUE;
    protected int [] MejorConjuntoSolucion;
    protected Random rd;
    
    protected abstract int [] generarCandidatos(ArrayList<Queue<Tarea>> cola);
    public abstract Pair<Integer,Integer> call();
    
    /**
     * Comprueba si en queda trabajos aun por asignar o trabajar con el.
     * @param Cola representa la estructura de datos que contiene los trabajos.
     * @return <b> FALSE </B> si no esta vacio y aun contiene tareas. <b> TRUE </b> si esta vacio.
     */
    public  boolean vacio(ArrayList<Queue<Tarea>> Cola){
        int index = 0;
        while(index < Cola.size()){
            if(!Cola.get(index).isEmpty()) return false;
            index++;
        }
        return true;
    }
    
    /**
     * Carga al algorimto con los datos del data set contenido en un fichero txt.
     * @param filer El nombre del fichero que contiene el data set.
     * @param seeds
     * 
     */
    public void cargarSimulacion(String filer,long seeds){
        try {
            this.rd = new Random(seeds);
            this.data = new DataSet(filer);
            this.numJobs = this.data.getNumJobs();
            this.numMaquinas = this.data.getNumMaquinas();
        } catch (IOException ex) {
            Logger.getLogger(Algoritmos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    protected int [] two_ops(int [] copia, int izq, int der){
        int [] salida = new int[copia.length];
        System.arraycopy(copia, 0, salida, 0, copia.length);
        salida[izq] = copia[der];
        salida[der] = copia[izq];
        return salida;
    }
}
