
package ALG_Geneticos;

import ALG.Algoritmos;
import EstructuraDatos.Cromosoma;
import EstructuraDatos.Resultados;
import EstructuraDatos.Tarea;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Queue;
import javafx.util.Pair;
import javax.swing.JFrame;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;


/**
 *
 * @author Hector Gonzalez Guerreiro.
 * 
 * Implementacion del algoimo genetico basico.
 * Inicialmente se creara el modelo Generacional. 
 * 
 */
public class Genetico_Basico extends Algoritmos{
    private int npoblacion,k,llamada;
     int [] per = {0,0,0,0,0,0,0};

    public Genetico_Basico(int npoblacion, int k) {
        this.npoblacion = npoblacion;
        this.k = k;
    }
    
   
  
    /**
     * Aqui se desarrolla el cruce OX
     * @param padre
     * @param madre
     * @param tama 
     * @return 
     */
    public Pair<Cromosoma,Cromosoma> cruceOX(Cromosoma padre, Cromosoma madre, int tama){
        int [] hijo_01 = new int[numJobs*numMaquinas], hijo_02 = new int[numJobs * numMaquinas];
        int [] h_01 = new int[numJobs], h_02 = new int[numJobs];
        int pos01 = this.rd.nextInt(numJobs*numMaquinas), pos02;
        float DerIzq = rd.nextFloat();
        
        
        for(int index = 0; index < numJobs; index++){ h_01[index] = 0; h_02[index] = 0;}
        for(int index = 0; index < numJobs*numMaquinas; index++){ hijo_01[index] = -1; hijo_02[index] = -1;}
        
        /* si es menor que 0.5 el subvector se toma de la parte izquierda */
        if(DerIzq < 0.5){
            int pos = pos01 - tama;
            if(pos < 0)
                pos02 = (numJobs*numMaquinas) + pos;
            else
                pos02 = pos;
        }
        /* si es mayot que 0.5 el subvector se toma de la parte derecha */
        else{
            int pos = pos01 + tama;
            if(pos >= (numJobs*numMaquinas))
                pos02 = pos % (numJobs*numMaquinas);
            else
                pos02 = pos;
        }
        
        /*Ya tenemos los dos pivotes de los cuales podemos tomar los trozos*/
        /* Hacemos un primer recorrido para generar los dos hijos y asignale parte de un progenitor*/
        
        for(int index = pos01; index < (tama + pos01); index++){
            if(index >= (numJobs*numMaquinas)){
                hijo_01[index % (numJobs*numMaquinas)] = padre.getCromosoma()[index % (numJobs*numMaquinas)];
                h_01[padre.getCromosoma()[index % (numJobs*numMaquinas)]]++;
            }
            else{
                hijo_01[index] = padre.getCromosoma()[index];
                h_01[padre.getCromosoma()[index]]++;
            }
        }
        
        for(int index = pos02; index < (tama - (numJobs * numMaquinas)); index++){
            if(index >= (numJobs*numMaquinas)){
                hijo_02[index % (numJobs*numMaquinas)] = padre.getCromosoma()[index % (numJobs*numMaquinas)];
                h_02[padre.getCromosoma()[index % (numJobs*numMaquinas)]]++;
            }
            else{
                hijo_02[index] = padre.getCromosoma()[index];
                h_02[padre.getCromosoma()[index]]++;
            }
        }
        
        /* A cada hijo se le completa informacion con la madre */
        
        int posicionHijo = 0,posicionPadre = 0;
        
        while(posicionHijo < (numJobs * numMaquinas)){
            if(hijo_01[posicionHijo] == -1){
                if(h_01[madre.getCromosoma()[posicionPadre]] < numMaquinas){
                    hijo_01[posicionHijo] = madre.getCromosoma()[posicionPadre];
                    h_01[madre.getCromosoma()[posicionPadre]]++;
                    posicionHijo++;
                    posicionPadre++;
                }
                else posicionPadre++;
            }
            else  posicionHijo++;
            //posicionPadre++;
        }
        
         posicionHijo = 0;
         posicionPadre = 0;
        
        while(posicionHijo < (numJobs * numMaquinas)){
            if(hijo_02[posicionHijo] == -1){
                if(h_02[madre.getCromosoma()[posicionPadre]] < numMaquinas){
                    hijo_02[posicionHijo] = madre.getCromosoma()[posicionPadre];
                    h_02[madre.getCromosoma()[posicionPadre]]++;
                    posicionHijo++;
                    posicionPadre++;
                }
                else posicionPadre++;
            }
            else posicionHijo++;
            //posicionPadre++;
        }
       
        hijo_01 = this.mutacion(hijo_01, Math.abs(rd.nextGaussian()));
        hijo_02 = this.mutacion(hijo_02, Math.abs(rd.nextGaussian()));
       
        int coste01,coste02;
        this.s.Simular(hijo_01, this.data.getColaTrabajos(), numJobs, numMaquinas);
        coste01 = this.s.getIteraciones();
        
        this.s.Simular(hijo_02, this.data.getColaTrabajos(), numJobs, numMaquinas);
        coste02 = this.s.getIteraciones();
        
         llamada = llamada + 2;
        return new Pair<>(new Cromosoma(hijo_01, coste01), new Cromosoma(hijo_02, coste02));
        
         
    }
    
    public int distacias(Cromosoma cromosoma_01, Cromosoma cromosoma_02){
        int sumatorio = 0;
        for(int index = 0; index < cromosoma_01.getCromosoma().length; index++)
            if(cromosoma_01.getCromosoma()[index] != cromosoma_02.getCromosoma()[index]) sumatorio++;
        return sumatorio;
    }
    
    private float calcularMediaFitnes(){
        int suma = 0;
        for(int index = 0; index <5; index++){
            suma += this.poblacion.get(index).getFitnes();
        }
        
        return suma / 5;
    }
    
    private float calcularVarianzaDistancia(){
        int suma = 0,n = 0;
        for(int index = 0; index < this.poblacion.size()-1; index = index + 2){
            //for(int index2 = index + 1; index2 < this.poblacion.size(); index2++){
                suma += this.distacias(this.poblacion.get(index), this.poblacion.get(index+1));
                n++;
           // }
        }
        
        int media =  suma / n;
        int sumatorio = 0;
        n = 0;
        
        for(int index = 0; index < this.poblacion.size()-1; index = index + 2){
            //for(int index2 = index + 1; index2 < this.poblacion.size(); index2++){
                sumatorio += Math.pow(this.distacias(this.poblacion.get(index), this.poblacion.get(index+1)) - media,2);
                n++;
           // }
        }
        
        return sumatorio / (n);
        
    }
    
    
    /**
     * Aplica una mutacion al conjunto solucion.
     * La mutacion consiste en generar un pivote que determine el inicio del subvector.
     * este pivote no debe ser superior a n-pivote > tamaño 
     * 
     * @return Devulve el conjunto solucion mutado.
     */
    private int [] mutacion(int [] cromosoma, double indiceMutacion){
        int porcentage = 0;
       
        if(indiceMutacion <= 0.4){
          porcentage = this.probabilidadesG[0];
          per[0]++;
        }
        else if(indiceMutacion <= 0.8){
          porcentage = this.probabilidadesG[1];
           per[1]++;
        }
        else if(indiceMutacion <= 1.2){
          porcentage = this.probabilidadesG[2];
           per[2]++;
        }
        else if(indiceMutacion <= 1.6){
          porcentage = this.probabilidadesG[3];
           per[3]++;
        }
        else if(indiceMutacion <= 2){
             per[4]++;
            porcentage = this.probabilidadesG[4];
        }
        else if (indiceMutacion <= 2.4){
             per[5]++;
             porcentage = this.probabilidadesG[5];
        }
        
        else if(indiceMutacion <= 2.8){
            per[6]++;
            porcentage = this.probabilidadesG[6];
        }
        
        
        int pos01 = this.rd.nextInt(cromosoma.length);
        int [] conjuntoNuevo = new int[cromosoma.length];
        //System.out.println(porcentage + " " + ((cromosoma.length * porcentage) / 100));
        int pos02 = ( pos01 + ((cromosoma.length * porcentage) / 100)) % cromosoma.length;
        
        System.arraycopy(cromosoma, 0, conjuntoNuevo, 0, cromosoma.length);
        ArrayList<Integer> posicionesPorEscoger = new ArrayList<>();
        
        int ele;
        for(int index = pos01; index < pos02; index++){
            if (pos01 > cromosoma.length) 
                ele = index % cromosoma.length;
            else 
                ele = index;
            posicionesPorEscoger.add(ele);
        }
        
        int elementoEs;
        int idex = pos01;
        while(!posicionesPorEscoger.isEmpty()){
            int pos = rd.nextInt(posicionesPorEscoger.size());
            elementoEs = posicionesPorEscoger.get(pos);
            posicionesPorEscoger.remove(pos);
            conjuntoNuevo[idex] = cromosoma[elementoEs];
            idex++;
        }
        
        return conjuntoNuevo;
    }

    
     /**
     * Comprueba si en queda trabajos aun por asignar o trabajar con el.
     * @param Cola representa la estructura de datos que contiene los trabajos.
     * @return <b> FALSE </B> si no esta vacio y aun contiene tareas. <b> TRUE </b> si esta vacio.
     */
    
    @Override
    public  boolean vacio(ArrayList<Queue<Tarea>> Cola){
        int index = 0;
        while(index < Cola.size()){
            if(!Cola.get(index).isEmpty()) return false;
            index++;
        }
        return true;
    }
    

    @Override
    public Resultados call() {
       this.Generar_Poblacion_Aleatoria(npoblacion);
       int mejor = Integer.MAX_VALUE, generacion = 0, noptimo = 5000;
       llamada = npoblacion;
       
       while(noptimo > generacion ){
          
           
           Pair<Cromosoma,Cromosoma> newHijos = this.seleccion();
           newHijos = this.cruceOX(newHijos.getKey(), newHijos.getValue(), this.rd.nextInt(numJobs*numMaquinas));
           this.generacionSig.add(newHijos.getKey());
           this.generacionSig.add(newHijos.getValue());
           
           generacion++;
       
           this.poblacion.sort((Cromosoma o1,Cromosoma o2) -> new Integer(o1.getFitnes()).compareTo(o2.getFitnes()));
           this.generacionSig.sort((Cromosoma o1,Cromosoma o2) -> new Integer(o1.getFitnes()).compareTo(o2.getFitnes()));

           for(int index = 5; index < this.poblacion.size() && index - 5 < this.generacionSig.size(); index++)
               this.poblacion.set(index, this.generacionSig.get(index - 5));
           
            this.poblacion.sort((Cromosoma o1,Cromosoma o2) -> new Integer(o1.getFitnes()).compareTo(o2.getFitnes()));
            
            if(this.poblacion.get(0).getFitnes() < mejor){
                mejor = this.poblacion.get(0).getFitnes();
                generacion = 0;

            }
            this.listaDistancias.add(this.calcularVarianzaDistancia());
            this.listaFitnees.add(this.calcularMediaFitnes());
            this.generacionSig.clear();
        }
       this.graficas();
        System.out.println(Arrays.toString(per));
       System.out.println(this.poblacion.get(0).getFitnes());
       return new Resultados(this.poblacion.get(0).getFitnes(), llamada, 0);
    }
    
     public void graficas(){
      

        JFreeChart Grafica = ChartFactory.createLineChart(String.valueOf(seeds) + " " + numJobs + " * " + numMaquinas,"Iteracion", "", this.Fitnees(),PlotOrientation.VERTICAL, true, true, false);
        
        ChartPanel Panel = new ChartPanel(Grafica);
        JFrame Ventana = new JFrame("JFreeChart");
        Ventana.getContentPane().add(Panel);
        Ventana.pack();
        Ventana.setVisible(true);
        Ventana.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        
        
        JFreeChart Grafica2 = ChartFactory.createLineChart(String.valueOf(seeds) + " " + numJobs + " * " + numMaquinas,"Iteracion", "", this.Distancia(),PlotOrientation.VERTICAL, true, true, false);
        
        ChartPanel Panel2 = new ChartPanel(Grafica2);
        JFrame Ventana2 = new JFrame("JFreeChart");
        Ventana2.getContentPane().add(Panel2);
        Ventana2.pack();
        Ventana2.setVisible(true);
        Ventana2.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        
    }
     
     private CategoryDataset Fitnees(){
           DefaultCategoryDataset Datos = new DefaultCategoryDataset();
        
        for(int index = 0; index < this.listaDistancias.size(); index++){
            Datos.addValue(this.listaFitnees.get(index), "Fitnes",new Float(index));
        }
        
        return Datos;
     }
     
     private CategoryDataset Distancia(){
           DefaultCategoryDataset Datos = new DefaultCategoryDataset();
        
        for(int index = 0; index < this.listaDistancias.size(); index++){
            Datos.addValue(this.listaDistancias.get(index), "Distancia",new Float(index));
        }
        
        return Datos;
     }
    
      /**
     * Metodo de seleccion por probabilidad. Los individuos se le asocia un porcentage
     * proporcional a su fitnes.
     * 
     * Para mejorar la diversidad crear un mecanismo para evitar el incesto. Intentar mezclar
     * con individuos peores.
     * 
     * @return Dos padres.
     */
    
    public Pair<Cromosoma,Cromosoma> seleccion(){
        ArrayList< Pair<Cromosoma,Integer>> listaIndividuo_coste = new ArrayList<>();
        int iteraciones = 0;
        Cromosoma p01 = null, p02 = null;
        
        while(iteraciones < this.poblacion.size() ){
            this.s.Simular(this.poblacion.get(iteraciones).getCromosoma(), this.data.getColaTrabajos(), numJobs, numMaquinas);
            listaIndividuo_coste.add(new Pair<>(this.poblacion.get(iteraciones),this.s.getIteraciones()));
            iteraciones++;
        }
        
        listaIndividuo_coste.sort((Pair<Cromosoma,Integer> o1, Pair<Cromosoma,Integer> o2) -> o1.getValue().compareTo(o2.getValue()));
        
        ArrayList<Float> lista_probabilidades = new ArrayList<>();
        float calculo = 0;
        
        
         for(int index = 1; index <= listaIndividuo_coste.size(); index++){
            if(listaIndividuo_coste.size() > 1) calculo = (float) ((0.5 - ((float)index / ((float)listaIndividuo_coste.size()+index))) * 100.0);
            lista_probabilidades.add(calculo);
        }
         
        //System.out.println(lista_probabilidades);
        iteraciones = 0;
        
        /* Eleccion del primer individuo */
        float valorProbabilidad = rd.nextFloat() * 100;
        boolean encontrado = false;
        while(iteraciones < listaIndividuo_coste.size() && !encontrado){
            if(valorProbabilidad  >= lista_probabilidades.get(iteraciones)) encontrado = true;
            else iteraciones++;
        }
        
        p01 = listaIndividuo_coste.get(iteraciones).getKey();
        
        lista_probabilidades.remove(iteraciones);
        listaIndividuo_coste.remove(iteraciones);
        iteraciones = 0;
        encontrado = false;
        valorProbabilidad = rd.nextFloat() * 100;
        
        while(iteraciones < listaIndividuo_coste.size() && !encontrado){
            if(valorProbabilidad >= lista_probabilidades.get(iteraciones)) encontrado = true;
            else iteraciones++;
        }
        
        if(iteraciones >= listaIndividuo_coste.size()) p02 = listaIndividuo_coste.get(0).getKey();
        else p02 = listaIndividuo_coste.get(iteraciones).getKey();
        
        return new Pair<>(p01,p02); 
        
    }
    
}
