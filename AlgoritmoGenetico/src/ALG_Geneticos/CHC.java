/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ALG_Geneticos;

import ALG.Algoritmos;
import EstructuraDatos.Cromosoma;
import EstructuraDatos.Resultados;
import EstructuraDatos.Tarea;
import java.util.ArrayList;
import java.util.Queue;
import javafx.util.Pair;
import javax.swing.JFrame;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 *
 * @author Hector
 */
public class CHC extends Algoritmos{
    private int npoblacion,k,llamada;

    public CHC(int npoblacion, int k) {
        this.npoblacion = npoblacion;
        this.k = k;
    }
    
    
       /**
     * Calculamos el fitness total.
     * Y mediante ruleta de posibilidad selecionamos a los 2 individuos.
     * 
     * @param k
     * @param lista
     * @return El par de progenitores.
     */
    public Pair<Cromosoma,Cromosoma> Seleccion(int k, ArrayList<Cromosoma> lista){
        return new Pair<>(Torne(k,lista),Torne(k,lista));
    }
    
    public Cromosoma Torne(int k,ArrayList<Cromosoma> lista){
        ArrayList<Cromosoma> podio = new ArrayList<>();
        
        for(int index = 0; index < k; index++){
            podio.add(lista.get(rd.nextInt(lista.size())));
        }
        
        podio.sort((Cromosoma o1,Cromosoma o2) -> new Integer(o1.getFitnes()).compareTo(o2.getFitnes()));
        return podio.get(0);
    }
    
    public Pair<Cromosoma,Cromosoma> cruceOX(Cromosoma padre, Cromosoma madre, int tama){
        int [] hijo_01 = new int[numJobs*numMaquinas], hijo_02 = new int[numJobs * numMaquinas];
        int [] h_01 = new int[numJobs], h_02 = new int[numJobs];
        int pos01 = this.rd.nextInt(numJobs*numMaquinas), pos02;
        float DerIzq = rd.nextFloat();
        
        for(int index = 0; index < numJobs; index++){ h_01[index] = 0; h_02[index] = 0;}
        for(int index = 0; index < numJobs*numMaquinas; index++){ hijo_01[index] = -1; hijo_02[index] = -1;}
        
        /* si es menor que 0.5 el subvector se toma de la parte izquierda */
        if(DerIzq < 0.5){
            int pos = pos01 - tama;
            if(pos < 0)
                pos02 = (numJobs*numMaquinas) + pos;
            else
                pos02 = pos;
        }
        /* si es mayot que 0.5 el subvector se toma de la parte derecha */
        else{
            int pos = pos01 + tama;
            if(pos >= (numJobs*numMaquinas))
                pos02 = pos % (numJobs*numMaquinas);
            else
                pos02 = pos;
        }
        
        /*Ya tenemos los dos pivotes de los cuales podemos tomar los trozos*/
        /* Hacemos un primer recorrido para generar los dos hijos y asignale parte de un progenitor*/
        
        for(int index = pos01; index < (tama + pos01); index++){
            if(index >= (numJobs*numMaquinas)){
                hijo_01[index % (numJobs*numMaquinas)] = padre.getCromosoma()[index % (numJobs*numMaquinas)];
                h_01[padre.getCromosoma()[index % (numJobs*numMaquinas)]]++;
            }
            else{
                hijo_01[index] = padre.getCromosoma()[index];
                h_01[padre.getCromosoma()[index]]++;
            }
        }
        
        for(int index = pos02; index < (tama - (numJobs * numMaquinas)); index++){
            if(index >= (numJobs*numMaquinas)){
                hijo_02[index % (numJobs*numMaquinas)] = padre.getCromosoma()[index % (numJobs*numMaquinas)];
                h_02[padre.getCromosoma()[index % (numJobs*numMaquinas)]]++;
            }
            else{
                hijo_02[index] = padre.getCromosoma()[index];
                h_02[padre.getCromosoma()[index]]++;
            }
        }
        
        /* A cada hijo se le completa informacion con la madre */
        
        int posicionHijo = 0,posicionPadre = 0;
        
        while(posicionHijo < (numJobs * numMaquinas)){
            if(hijo_01[posicionHijo] == -1){
                if(h_01[madre.getCromosoma()[posicionPadre]] <  numMaquinas){
                    hijo_01[posicionHijo] = madre.getCromosoma()[posicionPadre];
                    h_01[madre.getCromosoma()[posicionPadre]]++;
                    posicionHijo++;
                    posicionPadre++;
                }
                else posicionPadre++;
            }
            else  posicionHijo++;
            //posicionPadre++;
        }
        
         posicionHijo = 0;
         posicionPadre = 0;
        
        while(posicionHijo < (numJobs * numMaquinas)){
            if(hijo_02[posicionHijo] == -1){
                if(h_02[madre.getCromosoma()[posicionPadre]] <  numMaquinas){
                    hijo_02[posicionHijo] = madre.getCromosoma()[posicionPadre];
                    h_02[madre.getCromosoma()[posicionPadre]]++;
                    posicionHijo++;
                    posicionPadre++;
                }
                else posicionPadre++;
            }
            else posicionHijo++;
            //posicionPadre++;
        }
       
        hijo_01 = this.mutacion(hijo_01, Math.abs(rd.nextGaussian()));
        hijo_02 = this.mutacion(hijo_02, Math.abs(rd.nextGaussian()));
       
        int coste01,coste02;
        this.s.Simular(hijo_01, this.data.getColaTrabajos(), numJobs, numMaquinas);
        coste01 = this.s.getIteraciones();
        
        this.s.Simular(hijo_02, this.data.getColaTrabajos(), numJobs, numMaquinas);
        coste02 = this.s.getIteraciones();
        
        llamada = llamada + 2;
        return new Pair<>(new Cromosoma(hijo_01, coste01), new Cromosoma(hijo_02, coste02));
        
         
    }
    
    
    /**
     * Aplica una mutacion al conjunto solucion.
     * La mutacion consiste en generar un pivote que determine el inicio del subvector.
     * este pivote no debe ser superior a n-pivote > tamaño 
     * 
     * @return Devulve el conjunto solucion mutado.
     */
    private int [] mutacion(int [] cromosoma, double indiceMutacion){
        int porcentage;
      
        if(indiceMutacion <= 0.2)
          porcentage = this.probabilidadesG[0];
        else if(indiceMutacion <= 0.4)
          porcentage = this.probabilidadesG[1];
        else if(indiceMutacion <= 0.6)
          porcentage = this.probabilidadesG[2];
        else if(indiceMutacion <= 0.8)
          porcentage = this.probabilidadesG[3];
        else if(indiceMutacion <= 1.2)
          porcentage = this.probabilidadesG[4];
        else 
          porcentage = this.probabilidadesG[5];
        
        
        int pos01 = this.rd.nextInt(cromosoma.length);
        int [] conjuntoNuevo = new int[cromosoma.length];
        int pos02 = ( pos01 + ((cromosoma.length * porcentage) / 100)) % cromosoma.length;
        
        System.arraycopy(cromosoma, 0, conjuntoNuevo, 0, cromosoma.length);
        ArrayList<Integer> posicionesPorEscoger = new ArrayList<>();
        
        int ele;
        for(int index = pos01; index < pos02; index++){
            if (pos01 > cromosoma.length) 
                ele = index % cromosoma.length;
            else 
                ele = index;
            posicionesPorEscoger.add(ele);
        }
        
        int elementoEs;
        int idex = pos01;
        while(!posicionesPorEscoger.isEmpty()){
            int pos = rd.nextInt(posicionesPorEscoger.size());
            elementoEs = posicionesPorEscoger.get(pos);
            posicionesPorEscoger.remove(pos);
            conjuntoNuevo[idex] = cromosoma[elementoEs];
            idex++;
        }
        
        return conjuntoNuevo;
    }

    
     /**
     * Comprueba si en queda trabajos aun por asignar o trabajar con el.
     * @param Cola representa la estructura de datos que contiene los trabajos.
     * @return <b> FALSE </B> si no esta vacio y aun contiene tareas. <b> TRUE </b> si esta vacio.
     */
    
    @Override
    public  boolean vacio(ArrayList<Queue<Tarea>> Cola){
        int index = 0;
        while(index < Cola.size()){
            if(!Cola.get(index).isEmpty()) return false;
            index++;
        }
        return true;
    }
    
     public int distacias(Cromosoma cromosoma_01, Cromosoma cromosoma_02){
        int sumatorio = 0;
        for(int index = 0; index < cromosoma_01.getCromosoma().length; index++)
            if(cromosoma_01.getCromosoma()[index] != cromosoma_02.getCromosoma()[index]) sumatorio++;
        return sumatorio;
    }
    
    public ArrayList<Cromosoma> clearing(){
        ArrayList<Cromosoma> nuevoSugetos = new ArrayList<>();
        int dis, lider = 0;
       
        nuevoSugetos.add(this.poblacion.get(0));
        for(int index = 1; index < this.poblacion.size(); index++){
            dis = this.distacias(this.poblacion.get(lider),this.poblacion.get(index));
            
            if(dis > (numJobs*numMaquinas)/4){
                nuevoSugetos.add(this.poblacion.get(index));
                lider = index;
            }   
        }
        return nuevoSugetos;
    }
    
     public ArrayList<Pair<Cromosoma,Cromosoma>> ParejasValidas(int umbral){
        ArrayList<Cromosoma> nuevoSugetos = new ArrayList<>();
        ArrayList<Cromosoma> copia = (ArrayList<Cromosoma>) this.poblacion.clone();
        
        while(!copia.isEmpty()){
            int num = this.rd.nextInt(copia.size());
            nuevoSugetos.add(copia.get(num));
            copia.remove(num);
        }
        
        Cromosoma cr1,cr2;
        ArrayList<Pair<Cromosoma,Cromosoma>> listaParejas = new ArrayList<>();
        
        for(int index = 0; index <  nuevoSugetos.size() - 1; index = index + 2){
            cr1 =  nuevoSugetos.get(index);
            cr2 =  nuevoSugetos.get(index + 1);
            int dis = distacias(cr1, cr2);
            
            if( dis/2 > (umbral) ) 
                listaParejas.add(new Pair<>(cr1,cr2));
            
        }
        
        
        return listaParejas;
    }
     
     private float calcularMediaFitnes(){
        int suma = 0;
        for(int index = 0; index < this.poblacion.size(); index++){
            suma += this.poblacion.get(index).getFitnes();
        }
        
        return suma / this.poblacion.size();
    }
    
    private float calcularVarianzaDistancia(){
        int suma = 0,n = 0;
        for(int index = 0; index < this.poblacion.size()-1; index = index + 2){
            //for(int index2 = index + 1; index2 < this.poblacion.size(); index2++){
                suma += this.distacias(this.poblacion.get(index), this.poblacion.get(index+1));
                n++;
           // }
        }
        
        int media =  suma / n;
        int sumatorio = 0;
        n = 0;
        
        for(int index = 0; index < this.poblacion.size()-1; index = index + 2){
            //for(int index2 = index + 1; index2 < this.poblacion.size(); index2++){
                sumatorio += Math.pow(this.distacias(this.poblacion.get(index), this.poblacion.get(index+1)) - media,2);
                n++;
           // }
        }
        
        return sumatorio / (n);
        
    }

    @Override
    public Resultados call() {
       this.Generar_Poblacion_Aleatoria(npoblacion);
       int umbral = (numJobs * numMaquinas) / 4;
       int reinicio = 0;
       llamada = npoblacion;
       ArrayList<Cromosoma> poblacion_anterior = null;
       
       while(reinicio < 6 ){
           this.listaDistancias.add(this.calcularVarianzaDistancia());
           this.listaFitnees.add(this.calcularMediaFitnes());
           this.poblacion.sort((Cromosoma o1,Cromosoma o2) -> new Integer(o1.getFitnes()).compareTo(o2.getFitnes()));
           ArrayList<Pair<Cromosoma,Cromosoma>> listaParejas = this.ParejasValidas(umbral);

        for(int individuos = 0; individuos < listaParejas.size(); individuos++ ){
             Pair<Cromosoma,Cromosoma> newHijos = listaParejas.get(individuos);
             newHijos = this.cruceOX(newHijos.getKey(), newHijos.getValue(), this.distacias(newHijos.getKey(), newHijos.getValue()));
             this.generacionSig.add(newHijos.getKey());
             this.generacionSig.add(newHijos.getValue());

        }

        this.poblacion.sort((Cromosoma o1,Cromosoma o2) -> new Integer(o1.getFitnes()).compareTo(o2.getFitnes()));
        this.generacionSig.sort((Cromosoma o1,Cromosoma o2) -> new Integer(o1.getFitnes()).compareTo(o2.getFitnes()));

        for(int index = 0; index < this.poblacion.size() && index <this.generacionSig.size(); index++){
            if(this.poblacion.get(this.poblacion.size() - (index + 1)).getFitnes() > this.generacionSig.get(index).getFitnes()){
                this.poblacion.set(this.poblacion.size() - (index + 1), (Cromosoma) this.generacionSig.get(index).clone());
            }
        }

        this.poblacion.sort((Cromosoma o1,Cromosoma o2) -> new Integer(o1.getFitnes()).compareTo(o2.getFitnes()));
        if(poblacion_anterior != null) poblacion_anterior.sort((Cromosoma o1,Cromosoma o2) -> new Integer(o1.getFitnes()).compareTo(o2.getFitnes()));
        if(poblacion_anterior != null && this.poblacion.equals(poblacion_anterior)){
             umbral--;
         }
        
        if(umbral < 1){
             this.poblacion.sort((Cromosoma o1,Cromosoma o2) -> new Integer(o1.getFitnes()).compareTo(o2.getFitnes()));
             Cromosoma mejorCromosoma = this.poblacion.get(0);
             this.poblacion.clear();
             this.Generar_Poblacion_Aleatoria(npoblacion);
             this.poblacion.set(0, mejorCromosoma);
             reinicio++;
             umbral =  (numJobs * numMaquinas) / 4;
         }

           
            this.generacionSig.clear();
            poblacion_anterior = (ArrayList<Cromosoma>) this.poblacion.clone();
        }
       
       System.out.println(this.poblacion.get(0).getFitnes());
       graficas();
       return new Resultados(this.poblacion.get(0).getFitnes(), llamada, 0);
    }
    
     public void graficas(){
      

        JFreeChart Grafica = ChartFactory.createLineChart(String.valueOf(seeds) + " " + numJobs + " * " + numMaquinas,"Iteracion", "", this.Fitnees(),PlotOrientation.VERTICAL, true, true, false);
        
        ChartPanel Panel = new ChartPanel(Grafica);
        JFrame Ventana = new JFrame("JFreeChart");
        Ventana.getContentPane().add(Panel);
        Ventana.pack();
        Ventana.setVisible(true);
        Ventana.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        
        
        JFreeChart Grafica2 = ChartFactory.createLineChart(String.valueOf(seeds) + " " + numJobs + " * " + numMaquinas,"Iteracion", "", this.Distancia(),PlotOrientation.VERTICAL, true, true, false);
        
        ChartPanel Panel2 = new ChartPanel(Grafica2);
        JFrame Ventana2 = new JFrame("JFreeChart");
        Ventana2.getContentPane().add(Panel2);
        Ventana2.pack();
        Ventana2.setVisible(true);
        Ventana2.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        
    }
     
     private CategoryDataset Fitnees(){
           DefaultCategoryDataset Datos = new DefaultCategoryDataset();
        
        for(int index = 0; index < this.listaDistancias.size(); index++){
            Datos.addValue(this.listaFitnees.get(index), "Fitnes",new Float(index));
        }
        
        return Datos;
     }
     
     private CategoryDataset Distancia(){
           DefaultCategoryDataset Datos = new DefaultCategoryDataset();
        
        for(int index = 0; index < this.listaDistancias.size(); index++){
            Datos.addValue(this.listaDistancias.get(index), "Distancia",new Float(index));
        }
        
        return Datos;
     }
}
