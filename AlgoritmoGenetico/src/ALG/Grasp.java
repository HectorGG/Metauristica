package ALG;

import EstructuraDatos.DataSet;
import EstructuraDatos.Movimiento;
import EstructuraDatos.Resultados;
import EstructuraDatos.Tarea;
import Simulador.Greddy;
import Simulador.Simulador;
import java.util.ArrayList;
import java.util.Queue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.util.Pair;

/**
 *
 * @author Hector Gonzalez Guerreiro.
 * @since 18/04/2018
 *
 * Algoritmo GRASP
 *
 * La construcion de las soluciones Greedy probabilistico se hara usando un
 * greedy en el que se le proporciona un tamaño de una lista. Esta lista se
 * usara para escoger de forma aleatorio las tareas que contiene esa lista
 * siendo estas las tareas con el menor tiempo de ejecucion.
 *
 */
public class Grasp extends Algoritmos {

    private final int numGreedy;
    private final float tamaList;
    private Greddy GR = new Greddy();
    

    public Grasp(float tamaList, int numGreedy) {
        super();
        this.numGreedy = numGreedy;
        this.tamaList = tamaList;
    }

    private class Busqueda implements Callable<Pair<Integer, Integer>> {

        private final DataSet data;
        private int[] Solucion;
        private final Simulador s = new Simulador();
         private ArrayList<Movimiento> memoria = new ArrayList<>();

        public Busqueda(int[] Solucion, DataSet Data) {
            this.data = Data;
            this.Solucion = Solucion;
        }
        
        

        public Pair<Integer,Integer> call() {
        long startTime = System.currentTimeMillis();
        
        int iteracionesMejor ,iteracionesActual;
        int llamdaSimular = 1;
        s.Simular(Solucion, this.data.getColaTrabajos(), numJobs, numMaquinas);
        iteracionesMejor = s.getIteraciones();
        
        do{
            int index = 0;
            iteracionesActual = iteracionesMejor;
            ArrayList<Movimiento> listaVecinos = this.generadorVecinos(Solucion);
            memoria.clear();
            while(index < listaVecinos.size()){
                s.Simular(this.two_ops(Solucion, listaVecinos.get(index).getIzq(), listaVecinos.get(index).getDer()), (ArrayList<Queue<Tarea>>)this.data.getColaTrabajos().clone(), numJobs, numMaquinas);
                llamdaSimular++;
                if(s.getIteraciones() < iteracionesMejor){
                    iteracionesMejor = s.getIteraciones();
                    Solucion = s.getSolucionCandidata();
                }
                index++;
            }
           
        } while(iteracionesActual > iteracionesMejor);
        
       return new Pair<>(iteracionesMejor,llamdaSimular);
    }
    
    
   public ArrayList<Movimiento> generadorVecinos(int [] ConjuntoSolucion){
        int max = ((ConjuntoSolucion.length*ConjuntoSolucion.length) - (2 * ConjuntoSolucion.length));
        float numVecinos = (float) (max * (15.0 / 100.0));
        ArrayList<Movimiento> salida = new ArrayList<>();
        
        for(int index = 0; index < numVecinos; index++){
            Movimiento mv;
            do{
                mv = new Movimiento(rd.nextInt(ConjuntoSolucion.length), rd.nextInt(ConjuntoSolucion.length),0);
                
            }while(this.memoria.contains(mv) || mv.getDer() == mv.getIzq());
            this.memoria.add(mv);
            salida.add(mv);
            
        }
        return salida;
    }

        protected int[] two_ops(int[] copia, int izq, int der) {
            int[] salida = new int[copia.length];
            System.arraycopy(copia, 0, salida, 0, copia.length);
            salida[izq] = copia[der];
            salida[der] = copia[izq];
            return salida;
        }
    }

    @Override
    public int[] generarCandidatos(ArrayList<Queue<Tarea>> cola) {
        GR = new Greddy();
        GR.Greedy((ArrayList<Queue<Tarea>>) cola.clone(), numJobs, numMaquinas, (int) tamaList, this.rd);
        this.s.Simular(GR.getSolucionCandidata(), this.data.getColaTrabajos(), numJobs, numMaquinas);
        return GR.getSolucionCandidata();
    }

    @Override
    public Resultados call() {
        long startTime = System.currentTimeMillis();
        int llamada = 0;
        ArrayList<Future<Pair<Integer, Integer>>> Lista = new ArrayList<>();
        ExecutorService cal = Executors.newFixedThreadPool(5);

        for (int index = 0; index < numGreedy; index++) {
            Busqueda bus = new Busqueda(generarCandidatos(this.data.getColaTrabajos()), this.data);
            Lista.add(cal.submit(bus));
        }

        int iterM = Integer.MAX_VALUE;
        Pair<Integer, Integer> parSalida = null;

        for (int pos = 0; pos < Lista.size(); pos++) {
            try {
                if (Lista.get(pos).get().getKey() < iterM) {
                    parSalida = Lista.get(pos).get();
                    iterM = parSalida.getKey();
                }
            llamada = llamada + parSalida.getValue();
            } catch (InterruptedException | ExecutionException ex) {
                Logger.getLogger(Grasp.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return new Resultados(iterM, llamada, System.currentTimeMillis() - startTime);
    }

    
}
