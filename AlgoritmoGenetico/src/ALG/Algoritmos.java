
package ALG;

import EstructuraDatos.Cromosoma;
import EstructuraDatos.DataSet;
import EstructuraDatos.Resultados;
import EstructuraDatos.Tarea;
import Simulador.Simulador;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.util.Pair;

/**
 *
 * @author Hector Gonzalez Guerreiro
 * @since 30/03/2018
 * Gestor de los algoritmos de busqueda.
 *  
 */
public abstract class Algoritmos implements Callable<Resultados>{
    protected int numJobs,numMaquinas;
    protected Simulador s = new Simulador();
    protected DataSet data;
    protected Random rd;
    protected ArrayList<Cromosoma> poblacion = new ArrayList<>();
    protected final ArrayList<Cromosoma> generacionSig = new ArrayList<>();
    protected final int [] probabilidadesG = {1,3,5,6,8,9,10};
    protected final ArrayList<Float> listaDistancias = new ArrayList<>();
    protected final ArrayList<Float> listaFitnees = new ArrayList<>();
    protected long seeds;
    
    
    /**
     * Esta funcion permite la generacion de candidatos segun el algoritmo que
     * estamos usando.
     * @param T Represena la lista de trabajos.
     * @return Un conjunto candidato para una solucion.
     */
    public int[] generarCandidatos(ArrayList<Queue<Tarea>> T) {
        int[] conjuntoSolucion = new int[numJobs * numMaquinas];
        int num = 0;
        while (!vacio(T)) {
            int aleatorio = rd.nextInt(numJobs);
            if (!T.get(aleatorio).isEmpty()) {
                conjuntoSolucion[num] = T.get(aleatorio).poll().getTrabajo();
                num++;
            }
        }
        return conjuntoSolucion;
    }
    
    /**
     * Genera la primera poblacion aleatoria.
     * En este metodo se calcula el fitness.
     * @param num 
     */
    public void Generar_Poblacion_Aleatoria(int num){
        int iteraciones = 0;
        while(iteraciones < num ){
            this.s.Simular(generarCandidatos(this.data.getColaTrabajos()), this.data.getColaTrabajos(), numJobs, numMaquinas);
            this.poblacion.add(new Cromosoma(this.s.getSolucionCandidata(),this.s.getIteraciones()));
            iteraciones++;
        }
        
    }
    public abstract Resultados call();
    
    /**
     * Comprueba si en queda trabajos aun por asignar o trabajar con el.
     * @param Cola representa la estructura de datos que contiene los trabajos.
     * @return <b> FALSE </B> si no esta vacio y aun contiene tareas. <b> TRUE </b> si esta vacio.
     */
    public  boolean vacio(ArrayList<Queue<Tarea>> Cola){
        int index = 0;
        while(index < Cola.size()){
            if(!Cola.get(index).isEmpty()) return false;
            index++;
        }
        return true;
    }
    
    /**
     * Carga al algorimto con los datos del data set contenido en un fichero txt.
     * @param filer El nombre del fichero que contiene el data set.
     * @param seeds
     * 
     */
    public void cargarSimulacion(String filer,long seeds){
        try {
            this.rd = new Random(seeds);
            this.data = new DataSet(filer);
            this.numJobs = this.data.getNumJobs();
            this.numMaquinas = this.data.getNumMaquinas();
            this.seeds = seeds;
        } catch (IOException ex) {
            Logger.getLogger(Algoritmos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * 
     * @param copia
     * @param izq
     * @param der
     * @return 
     */
    protected int [] two_ops(int [] copia, int izq, int der){
        int [] salida = new int[copia.length];
        System.arraycopy(copia, 0, salida, 0, copia.length);
        salida[izq] = copia[der];
        salida[der] = copia[izq];
        return salida;
    }
    
    
     /**
     * @param k
     * @return El par de progenitores.
     */
    protected Pair<Cromosoma,Cromosoma> SeleccionTorneo(int k){
        return new Pair<>(Torne(k),Torne(k));
    }
    
    /**
     * Consiste en selecionar k elementos de los cuales se escoje el mejor de entre ellos.
     * @param k Cuantos elementos considero para hacer el torneo.
     * @return El ganador del torneo.
     */
    protected Cromosoma Torne(int k){
        ArrayList<Cromosoma> podio = new ArrayList<>();
        
        for(int index = 0; index < k; index++){
            podio.add(this.poblacion.get(rd.nextInt(this.poblacion.size())));
        }
        
        podio.sort((Cromosoma o1,Cromosoma o2) -> new Integer(o1.getFitnes()).compareTo(o2.getFitnes()));
        return podio.get(0);
    }
    
    /**
     * Consiste en seleccionar dos sujetos con una ruleta de probabilidad inveresa.
     * @return la pareja de la cual se le aplica el cruce. 
     */
   /* protected  Pair<Cromosoma,Cromosoma> SeleccionRuletaProbabilidad(){
        
    }*/
    
    
    
}
