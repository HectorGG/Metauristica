package ALG;

import EstructuraDatos.Movimiento;
import EstructuraDatos.Resultados;
import EstructuraDatos.Tarea;
import java.util.ArrayList;
import java.util.Queue;
import javafx.util.Pair;

/**
 *
 * @author Hector Gonzalez Guerreiro.
 * @since 25/04/2018
 * 
 * Algoritmo VNN
 * 
 * 
 */
public class Vns extends Algoritmos{
    private final int id, Kmax, Blmax;
    private int llamadaSimular = 0;
    private int [] ConjuntoS;
    private final ArrayList<Movimiento> memoria = new ArrayList<>();
    
    
    public Vns(int Blmax,int Kmax,int id){
        super();
        this.Blmax = Blmax; 
        this.Kmax = Kmax;
        this.id = id;
    }
    
    @Override
    public int[] generarCandidatos(ArrayList<Queue<Tarea>> cola) {
        int[] conjuntoSolucion = new int[numJobs * numMaquinas];
        int num = 0;
        while (!this.vacio(cola)) {
            int aleatorio = rd.nextInt(numJobs);
            if (!cola.get(aleatorio).isEmpty()) {
                conjuntoSolucion[num] = cola.get(aleatorio).poll().getTrabajo();
                num++;
            }
        }
        return conjuntoSolucion;
    }
    
   

    @Override
    public Resultados call() {
       long startTime = System.currentTimeMillis();
       int [] conjuntoActual = generarCandidatos(this.data.getColaTrabajos());
       int k = 1, bl = 0;
       int [] conjuntoCandidato; 
       Pair<Integer,Integer> par = this.busqueda(conjuntoActual);
       int costeActual = par.getKey();
       
       
       while(bl < this.Blmax ){
           if(k > Kmax) k = 1;
           
           conjuntoCandidato = mutacion(k);

           par = this.busqueda(conjuntoCandidato);
           bl++;
           
           if(costeActual > par.getKey()){
               costeActual = par.getKey();
               this.ConjuntoS = conjuntoCandidato;
               k = 1;
           }
           
           else k++;
           llamadaSimular = llamadaSimular + par.getValue();
       }
       return new Resultados(costeActual, llamadaSimular, System.currentTimeMillis() - startTime);
      
       
    }
    
    /**
     * Aplica una mutacion al conjunto solucion.
     * La mutacion consiste en generar un pivote que determine el inicio del subvector.
     * este pivote no debe ser superior a n-pivote > tamaño 
     * 
     * @return Devulve el conjunto solucion mutado.
     */
    private int [] mutacion(int k){
        int tama = this.ConjuntoS.length - (this.ConjuntoS.length / (9 - k));
        int posi = this.rd.nextInt(tama);
        int [] conjuntoNuevo = new int[this.ConjuntoS.length];
        ArrayList<Integer> posicionesPorEscoger = new ArrayList<>();
        
        for(int index = posi; index < posi + (this.ConjuntoS.length / (9 - k)); index++) posicionesPorEscoger.add(index);
       
        System.arraycopy(this.ConjuntoS, 0, conjuntoNuevo, 0, ConjuntoS.length);
        
        int idex = posi;
        while(!posicionesPorEscoger.isEmpty()){
            int pos = rd.nextInt(posicionesPorEscoger.size()); //
            int ele = posicionesPorEscoger.get(pos);
            posicionesPorEscoger.remove(pos);
           // System.out.println("Es el idex" + idex + "   ele:" + ele);
            conjuntoNuevo[idex] = this.ConjuntoS[ele];
            idex++;
        }
        
        return conjuntoNuevo;
                 
        
    }
    

    public Pair<Integer,Integer> busqueda(int [] Solucion) {
        
        int iteracionesMejor,iteracionesActual;
        int llamdaSimular = 1;
        s.Simular(Solucion, this.data.getColaTrabajos(), numJobs, numMaquinas);
        iteracionesMejor = s.getIteraciones();
        
        do{
            int index = 0;
            iteracionesActual = iteracionesMejor;
            ArrayList<Movimiento> listaVecinos = this.generadorVecinos(Solucion);
            memoria.clear();
            while(index < listaVecinos.size()){
                s.Simular(this.two_ops(Solucion, listaVecinos.get(index).getIzq(), listaVecinos.get(index).getDer()), (ArrayList<Queue<Tarea>>)this.data.getColaTrabajos().clone(), numJobs, numMaquinas);
                llamdaSimular++;
                if(s.getIteraciones() < iteracionesMejor){
                    iteracionesMejor = s.getIteraciones();
                    Solucion = s.getSolucionCandidata();
                }
                index++;
            }
        } while(iteracionesActual > iteracionesMejor);
        
       ConjuntoS = Solucion;
       return new Pair<>(iteracionesMejor,llamdaSimular);
    }
    
    
   public ArrayList<Movimiento> generadorVecinos(int [] ConjuntoSolucion){
        int max = ((ConjuntoSolucion.length*ConjuntoSolucion.length) - (2 * ConjuntoSolucion.length));
        float numVecinos = (float) (max * (15.0 / 100.0));
        System.out.println(numVecinos);
        System.out.println(max);
        ArrayList<Movimiento> salida = new ArrayList<>();
        
        for(int index = 0; index < numVecinos; index++){
            Movimiento mv;
            do{
                mv = new Movimiento(rd.nextInt(ConjuntoSolucion.length), rd.nextInt(ConjuntoSolucion.length),0);
                
            }while(this.memoria.contains(mv) || mv.getDer() == mv.getIzq());
            this.memoria.add(mv);
            salida.add(mv);
            
        }
        return salida;
    }
    
   
     
}
