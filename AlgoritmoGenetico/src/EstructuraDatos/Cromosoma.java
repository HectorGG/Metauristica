
package EstructuraDatos;

/**
 *
 * @author Hector Gonzalez Guerreiro.
 * @since 27/05/2018
 * 
 */

public class Cromosoma {
   private final int identificador;
   private final int [] cromosoma;
   private final int fitnes;
   
   public static int numindividuos = 0;
   
   
   public Cromosoma(int [] cromosoma, int fitnes){
       this.identificador = numindividuos;
       this.cromosoma = cromosoma;
       this.fitnes = fitnes;
       numindividuos++;
   }

    public int getIdentificador() {
        return identificador;
    }

    public int[] getCromosoma() {
        return cromosoma;
    }

    public int getFitnes() {
        return fitnes;
    }
    
    public Object clone(){
        int [] cr = new int [this.cromosoma.length];
        System.arraycopy(this.cromosoma, 0, cr, 0, cr.length);
        Cromosoma crnuevo = new Cromosoma(cr, this.fitnes);
        return crnuevo;  
    }
    
    public boolean equals(Object op2){
        int index = 0; 
        Cromosoma op = (Cromosoma) op2;
        while(index < this.cromosoma.length){
            if(this.cromosoma[index] != op.cromosoma[index]) return false;
            else index++;
        }
        return true;
    }
    
   /*
    public boolean familia(Cromosoma p01) {
        System.out.print(this.getPadres()[0] + " " + this.getPadres()[1] + " " + p01.getIdentificador()  );
        boolean salida;
        if(this.getPadres()[0] == p01.getIdentificador() || this.getPadres()[1] == p01.getIdentificador()) return true;
        else if(this.padres[0] != -1 || this.padres[1] != -1){
                if(this.getPadres()[0] == p01.getPadres()[0] || this.getPadres()[0] == p01.getPadres()[1]  || this.getPadres()[1] == p01.getPadres()[0] || this.getPadres()[1] == p01.getPadres()[1] ){
                    System.out.println("true");
                    return true;
                }
        }
        System.out.println("false");
        return false;
    }
   */
   
}
