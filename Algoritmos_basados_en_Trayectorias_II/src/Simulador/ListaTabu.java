
package Simulador;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Hector Gonzalez Guerreiro.
 * Estructura ternaria con 3 componentes, los dos primeros componentes corresponde 
 * con las posiciones de los eelementos, el 3 corresponde con el coste de ese movimiento.
 * 
 */
public class ListaTabu {
    private final ArrayList<Movimiento> elementos;
    private final int capacidad;
    private int [] conjuntoSolucion;

    public void add(Integer izq, Integer der, int iteraciones, int[] conjuntoSolucion) {
        elementos.add(new Movimiento(izq, der, iteraciones));
        this.conjuntoSolucion = new int[conjuntoSolucion.length];
        System.arraycopy(conjuntoSolucion, 0, this.conjuntoSolucion, 0, this.conjuntoSolucion.length);
        Collections.sort(elementos, ( Movimiento o1,  Movimiento o2) -> {
                return new Integer(o1.getCoste()).compareTo(o2.getCoste());
        });
    }
    
    public ListaTabu(int capacidad){
        this.elementos = new ArrayList<>(capacidad);
        this.capacidad = capacidad;
    }
    
    public void add(Movimiento ele){
        int pos = 0;
        boolean encontrado = false;
        
        while(pos < elementos.size() && !encontrado){
            if(elementos.get(pos).getCoste() > ele.getCoste()){
                encontrado = true;
            }
            else 
                pos++;
        }
        
        if(encontrado){
            for(int desplazamiento = pos; desplazamiento < this.capacidad; desplazamiento++ ){
                Movimiento aux = elementos.get(pos);
                elementos.set(pos, ele);
                ele = aux;
            }
        }
        
        
    }
    
    public void getElementos(int Izq, int Der, int Cost, int index){
        Izq = elementos.get(index).getIzq();
        Der = elementos.get(index).getDer();
        Cost = elementos.get(index).getCoste();
    }
    
    public int getNumElementos(){
        return elementos.size();
    }
    
    public int getSize(){
        return elementos.size();
    }
    
    public boolean existeMovimiento(Movimiento mv){
        int pivote = 0;
        while(pivote < this.elementos.size()){
            if(mv.equals(elementos.get(pivote))) return true;
            pivote++;
        }
        return false;
    }
    
    
    
   
}
