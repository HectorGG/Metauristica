
package ALG;

import EstructuraDatos.Tarea;
import java.util.ArrayList;
import java.util.Queue;
import javafx.util.Pair;

/**
 *
 * @author Hector Gonzalez Guerreiro.
 * 
 * Este algoritmo de busqueda local genera vecinos hasta que el vecino que genera el mejor que 
 * el cadidato actual, con lo cual deja de generar mas vecinos y lo sustituye como solucion 
 * actual, este algoritmo es muchisimo mas rapido que el de busquedaLocal.
 * 
 */

public class BusquedaLocalPM extends Algoritmos{
    String nombre = "BusquedaLocalPV"; 
    private final int id;
    public BusquedaLocalPM(int id){
        super();
        this.id = id;
    }
    
     /**
     * Esta funcion permite la generacion de candidatos segun el algoritmo que
     * estamos usando.
     * @param T Represena la lista de trabajos.
     * @return Un conjunto candidato para una solucion.
     */
    
    @Override
    public int[] generarCandidatos(ArrayList<Queue<Tarea>> T) {
        int[] conjuntoSolucion = new int[numJobs * numMaquinas];
        int num = 0;
        while (!this.vacio(T)) {
            int aleatorio = rd.nextInt(numJobs);
            if (!T.get(aleatorio).isEmpty()) {
                conjuntoSolucion[num] = T.get(aleatorio).poll().getTrabajo();
                num++;
            }
        }
        return conjuntoSolucion;
    }
     

    @Override
    public Pair<Integer,Integer> call() {
        int iteracionesMejor ,indiceIzq ,indiceDer,iteracionesActual;
        int [] Solucion = generarCandidatos((ArrayList<Queue<Tarea>>)this.data.getColaTrabajos().clone());
        int llamdaSimular = 1;
        s.Simular(Solucion, this.data.getColaTrabajos(), numJobs, numMaquinas);
        iteracionesMejor = s.getIteraciones();
        
        do{
            boolean encontrado = false;
            int index = 0;
            iteracionesActual = iteracionesMejor;
            while(!encontrado && index < (Solucion.length*Solucion.length) - 2*Solucion.length){
                indiceIzq = index / Solucion.length;
                indiceDer = index % Solucion.length;
                if(indiceDer != indiceIzq &&  indiceDer > indiceIzq){
                    s.Simular(this.two_ops(Solucion, indiceIzq, indiceDer), (ArrayList<Queue<Tarea>>)this.data.getColaTrabajos().clone(), numJobs, numMaquinas);
                    llamdaSimular++;
                    if(s.getIteraciones() < iteracionesMejor){
                        iteracionesMejor = s.getIteraciones();
                        Solucion = s.getSolucionCandidata();
                        encontrado = true;
                    }
                     
                }
                index++;
            }
            dat.add(new Pair<>(id,new Pair<>(iteracionesMejor,llamdaSimular)));
        }while(iteracionesActual > iteracionesMejor);
       
        return new Pair<>(iteracionesMejor,llamdaSimular);
    }
    
}
