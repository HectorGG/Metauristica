package ALG;

import EstructuraDatos.Tarea;
import java.util.ArrayList;
import java.util.Queue;
import javafx.util.Pair;

/**
 *
 * @author Hector Gonzalez Guerreiro.
 * @since 18/04/2018
 * 
 * Algoritmo ILS
 * 
 * 
 */
public class Ils extends Algoritmos{
    private int llamadaSimular = 0;
    private final int id;
    private final int indiceMutacion;
    private final int numVeces;
    private int [] ConjuntoS;
    
    
    public Ils(int numV,int indiceMutacion,int id){
        super();
        this.numVeces = numV; 
        this.indiceMutacion = indiceMutacion;
        this.id = id;
    }
    
    @Override
    protected int[] generarCandidatos(ArrayList<Queue<Tarea>> cola) {
        int[] conjuntoSolucion = new int[numJobs * numMaquinas];
        int num = 0;
        while (!this.vacio(cola)) {
            int aleatorio = rd.nextInt(numJobs);
            if (!cola.get(aleatorio).isEmpty()) {
                conjuntoSolucion[num] = cola.get(aleatorio).poll().getTrabajo();
                num++;
            }
        }
        return conjuntoSolucion;
    }

    @Override
    public Pair<Integer, Integer> call() {
       int [] conjuntoActual = generarCandidatos(this.data.getColaTrabajos());
       Pair<Integer,Integer> par = this.busqueda(conjuntoActual);
       int costeActual = par.getKey(), numVeces = 0;
       
       
       while(numVeces < this.numVeces ){
           conjuntoActual = mutacion();
           System.out.println(conjuntoActual.length);
           par = this.busqueda(conjuntoActual);
           //dat.add(new Pair<>(id,new Pair<>(par.getKey(),llamadaSimular)));
           if(costeActual > par.getKey()){
               costeActual = par.getKey();
           }
           System.out.println("Coste: " + costeActual);
           numVeces++;
           
       }
       
       return new Pair<>(costeActual,llamadaSimular);
      
       
    }
    
    /**
     * Aplica una mutacion al conjunto solucion.
     * La mutacion consiste en generar un pivote que determine el inicio del subvector.
     * este pivote no debe ser superior a n-pivote > tamaño 
     * 
     * @return Devulve el conjunto solucion mutado.
     */
    private int [] mutacion(){
        int tama = this.ConjuntoS.length - (this.ConjuntoS.length / this.indiceMutacion);
        int posi = this.rd.nextInt(tama);
        int [] conjuntoNuevo = new int[this.ConjuntoS.length];
        ArrayList<Integer> posicionesPorEscoger = new ArrayList<>();
        
        for(int index = posi; index < posi + (this.ConjuntoS.length / this.indiceMutacion); index++) posicionesPorEscoger.add(index);
       
        System.arraycopy(this.ConjuntoS, 0, conjuntoNuevo, 0, ConjuntoS.length);
        
        int idex = posi;
        while(!posicionesPorEscoger.isEmpty()){
            int pos = rd.nextInt(posicionesPorEscoger.size());
            int ele = posicionesPorEscoger.get(pos);
            posicionesPorEscoger.remove(pos);
            conjuntoNuevo[idex] = this.ConjuntoS[ele];
            idex++;
        }
        
        return conjuntoNuevo;
                 
        
    }
    
    
    /**
     * Apartir de la solucion generada por el Greedy se realiza una busqueda local.
     * @param Solucion Conjunto solucioin proporcionada por el greedy.
     * @return Par iteraciones y evaluaxion.
     */
    public Pair<Integer,Integer> busqueda(int[] Solucion) {
        int iteracionesMejor ,indiceIzq ,indiceDer,iteracionesActual;
        llamadaSimular++;
        s.Simular(Solucion, this.data.getColaTrabajos(), numJobs, numMaquinas);
        iteracionesMejor = s.getIteraciones();

        do{
            boolean encontrado = false;
            int index = 0;
            iteracionesActual = iteracionesMejor;
            while(!encontrado && index < (Solucion.length*Solucion.length) - 2*Solucion.length){
                indiceIzq = index / Solucion.length;
                indiceDer = index % Solucion.length;
                if(indiceDer != indiceIzq &&  indiceDer > indiceIzq){
                    s.Simular(this.two_ops(Solucion, indiceIzq, indiceDer), (ArrayList<Queue<Tarea>>)this.data.getColaTrabajos().clone(), numJobs, numMaquinas);
                    llamadaSimular++;
                    if(s.getIteraciones() < iteracionesMejor){
                        iteracionesMejor = s.getIteraciones();
                        Solucion = s.getSolucionCandidata();
                        encontrado = true;
                    }

                }
                //System.out.println(index);
                index++;
            }
           // dat.add(new Pair<>(id,new Pair<>(iteracionesMejor,llamdaSimular)));
        }while(iteracionesActual > iteracionesMejor);
        ConjuntoS = Solucion;
        dat.add(new Pair<>(id,new Pair<>(iteracionesMejor,llamadaSimular)));
        return new Pair<>(iteracionesMejor,llamadaSimular);
    }
   
    
}
