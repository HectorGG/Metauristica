package ALG;

import EstructuraDatos.Tarea;
import Simulador.Greddy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Queue;
import javafx.util.Pair;

/**
 *
 * @author Hector Gonzalez Guerreiro.
 * @since 18/04/2018
 * 
 * Algoritmo GRASP 
 * 
 * La construcion de las soluciones Greedy probabilistico se hara usando un greedy en el que
 * se le proporciona un tamaño de una lista. Esta lista se usara para escoger de forma aleatorio
 * las tareas que contiene esa lista siendo estas las tareas con el menor tiempo de ejecucion.
 * 
 */
public class Grasp extends Algoritmos{
    private final int numGreedy;
    private final float  tamaList;
    private Greddy GR = new Greddy();
    private int llamadaSimular = 0,id;
    
    
    public Grasp(float tamaList, int numGreedy,int id){
        super();
        this.numGreedy = numGreedy;
        this.tamaList = tamaList;
        this.id = id;
    }
    
    @Override
    protected int[] generarCandidatos(ArrayList<Queue<Tarea>> cola) {
        GR = new Greddy();
        GR.Greedy((ArrayList<Queue<Tarea>>) cola.clone(), numJobs, numMaquinas, 5, this.rd);
        return GR.getSolucionCandidata();
    }

    @Override
    public Pair<Integer, Integer> call() {
        ArrayList<Pair<Integer,Integer>> datos = new ArrayList<>();
        for(int index = 0; index < numGreedy; index++){
            datos.add(busqueda(generarCandidatos(this.data.getColaTrabajos())));
        }
        
        int iterM = Integer.MAX_VALUE;
        Pair<Integer,Integer> parSalida = null;
        for(int index = 0; index < datos.size(); index++){
            if(datos.get(index).getKey() < iterM)
                parSalida = datos.get(index);
        }
        //dat.add(new Pair<>(id,new Pair<>(iteracionesMejor,llamadaSimular)));
        return parSalida;
    }
    
   
    
    /**
     * Apartir de la solucion generada por el Greedy se realiza una busqueda local.
     * @param Solucion Conjunto solucioin proporcionada por el greedy.
     * @return Par iteraciones y evaluaxion.
     */
    public Pair<Integer,Integer> busqueda(int[] Solucion) {
        int iteracionesMejor ,indiceIzq ,indiceDer,iteracionesActual;
        llamadaSimular++;
        s.Simular(Solucion, this.data.getColaTrabajos(), numJobs, numMaquinas);
        iteracionesMejor = s.getIteraciones();
        
        do{
            boolean encontrado = false;
            int index = 0;
            iteracionesActual = iteracionesMejor;
            while(!encontrado && index < (Solucion.length*Solucion.length) - 2*Solucion.length){
                indiceIzq = index / Solucion.length;
                indiceDer = index % Solucion.length;
                if(indiceDer != indiceIzq &&  indiceDer > indiceIzq){
                    s.Simular(this.two_ops(Solucion, indiceIzq, indiceDer), (ArrayList<Queue<Tarea>>)this.data.getColaTrabajos().clone(), numJobs, numMaquinas);
                    llamadaSimular++;
                    if(s.getIteraciones() < iteracionesMejor){
                        iteracionesMejor = s.getIteraciones();
                        Solucion = s.getSolucionCandidata();
                        encontrado = true;
                    }
                     
                }
                index++;
            }
            
        }while(iteracionesActual > iteracionesMejor);
        dat.add(new Pair<>(id,new Pair<>(iteracionesMejor,llamadaSimular)));
        return new Pair<>(iteracionesMejor,llamadaSimular);
    }
   
    
}
